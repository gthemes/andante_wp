	<div class="yo-options yo-container">


		<div id="yo-popup-save" class="yo-save-popup">
			<div class="yo-save-save">Options Updated</div>
		</div>
		
		<div id="yo-popup-reset" class="yo-save-popup">
			<div class="yo-save-reset">Options Reset</div>
		</div>
		
		<div id="yo-popup-fail" class="yo-save-popup">
			<div class="yo-save-fail">Error!</div>
		</div>
		
		<span style="display: none;" id="hooks"></span>
		<input type="hidden" id="reset" value="<?php if(isset($_REQUEST['reset'])) echo $_REQUEST['reset']; ?>" />
		<input type="hidden" id="security" name="security" value="<?php echo wp_create_nonce('yo_ajax_nonce'); ?>" />

		<form id="yo-form" method="post" action="<?php echo esc_attr( $_SERVER['REQUEST_URI'] ) ?>" enctype="multipart/form-data" >
		
			<div class="yo-header">
			
				<div class="yo-logo">
					<h2><?php _e('Theme Options', 'theme_admin'); ?></h2>
				</div>
			
				<div id="js-warning"><?php _e('Warning - This options panel will not work properly without javascript!', 'theme_admin'); ?></div>
				<div class="icon-option"></div>
				<div class="clear"></div>
			
	    	</div>

			<div class="yo-infobar">
			
				<a>
					<div class="yo-expand expand">Expand</div>
				</a>
							
				<img style="display:none" src="<?php echo YO_IMG_URL; ?>loading-bottom.gif" class="ajax-loading-img ajax-loading-img-bottom" alt="Working..." />

				<button type="button" class="button-primary yo-save">
					<?php _e('Save All Changes', 'theme_admin'); ?>
				</button>
				
			</div><!-- .yo-infobar --> 	
			
			<div class="yo-content">
			
				<div class="yo-nav">
					<ul class="yo-tabs">
					  <?php echo $theme_options_tabs; ?>
					</ul>
				</div>

				<div class="yo-sections">
			  		<?php echo $theme_options_fields; ?>
			  	</div>
			  	
				<div class="clear"></div>
				
			</div>
			
			<div class="yo-savebar"> 
			
				<img style="display:none" src="<?php echo YO_IMG_URL; ?>loading-bottom.gif" class="ajax-loading-img ajax-loading-img-bottom" alt="Working..." />
				<button type="button" class="yo-save yo-submit button-primary"><?php _e('Save All Changes', 'theme_admin');?></button>			
				<button id ="yo-reset" type="button" class="button submit-button reset-button" ><?php _e('Options Reset', 'theme_admin');?></button>
				<img style="display:none" src="<?php echo YO_IMG_URL; ?>loading-bottom.gif" class="ajax-reset-loading-img ajax-loading-img-bottom" alt="Working..." />
				
			</div><!-- .yo-savebar --> 
	 
		</form>
		
		<div style="clear:both;"></div>

	</div><!-- #yo-container -->