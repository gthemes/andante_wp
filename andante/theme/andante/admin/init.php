<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }
/*
Title		: Yow Framework - Generator of Fields, Meta Boxes, Theme Options, Shortcodes
Description	: Yow is an extendable WordPress admin suite that helps generating form fields, meta boxes, theme options and shortcodes.
Version		: 1.0.0
Author		: Giordano Piazza
Author URI	: http://giordanopiazza.com
License		: GPLv2+
Credits		: Meta Box Script - http://www.deluxeblogtips.com/meta-box/
			  Slightly Modified Options Framework - https://github.com/sy4mil/Options-Framework
			  Thematic Options Panel - http://wptheming.com/2010/11/thematic-options-panel-v2/
		 	  Woo Themes - http://woothemes.com/
		 	  Option Tree - http://wordpress.org/extend/plugins/option-tree/
*/



/**
 *------------------------------------------------------------------------------------------------
 * Configuration
 *------------------------------------------------------------------------------------------------
 *
 * To override the default values, define the constants before including the 'init.php' file.
 *
 */

// Script version
define('YO_VER', '1.0.0');

// Fields prefix (Used for meta data entries and as the theme options array prefix)
if ( ! defined('YO_PREFIX'))
	define('YO_PREFIX', 'yo_');

// Define the ID key to store theme options
// TODO: change name to YO_OPTIONS
if ( ! defined('THEME_OPTIONS'))
	define('THEME_OPTIONS', YO_PREFIX.'theme_options');

// Define plugin URLs, for fast enqueuing scripts and styles
if ( ! defined('YO_URL'))
	define('YO_URL', trailingslashit(get_template_directory_uri().'/admin'));

if ( ! defined('YO_JS_URL'))
	define('YO_JS_URL', trailingslashit(YO_URL.'assets/js'));

if ( ! defined('YO_CSS_URL'))
	define('YO_CSS_URL', trailingslashit(YO_URL.'assets/css'));

if ( ! defined('YO_IMG_URL'))
	define('YO_IMG_URL', trailingslashit(YO_URL.'assets/img'));

// Plugin paths, for including files
if ( ! defined('YO_DIR'))
	define('YO_DIR', plugin_dir_path(__FILE__));

if ( ! defined('YO_INC_DIR'))
	define('YO_INC_DIR', trailingslashit(YO_DIR));

if ( ! defined('YO_FIELDS_DIR'))
	define('YO_FIELDS_DIR', trailingslashit(YO_INC_DIR.'fields'));

if ( ! defined('YO_LIB'))
	define('YO_LIB', trailingslashit(YO_INC_DIR.'library'));

// A global array to store the multiple/cloneable fields
global $yo_multiple_fields;
$yo_multiple_fields = array();


// Load the plugin files ONLY in the admin area
if (defined('WP_ADMIN') && WP_ADMIN)
// if (is_admin())
{
	// Load admin language file
	// $locale = get_locale();
	// $dir    = trailingslashit(YO_DIR . 'languages');
	// $mofile = "{$dir}{$locale}.mo";
	// In themes/current-theme/admin/ directory
	// load_theme_textdomain('theme_admin', $mofile);

	load_theme_textdomain('theme_admin', get_template_directory() . '/languages');
    $locale = get_locale();
    $locale_file = get_template_directory() . "/languages/$locale.php";
    if (is_readable($locale_file))
        require_once($locale_file);

	// Include field classes
	foreach (glob(YO_FIELDS_DIR.'*.php') as $file)
		require_once $file;

	// Include main files
	require_once YO_LIB.'fields.php';
	require_once YO_LIB.'metabox.php';
	require_once YO_LIB.'options.php';
	require_once YO_LIB.'shortcodes.php';
}



/**
 *------------------------------------------------------------------------------------------------
 * Register Fields
 *------------------------------------------------------------------------------------------------
 *
 * Helper function that stores the multiple/cloneable field names in one array. This is done to
 * differentiate single (strings) and multiple (arrays) items when using them in theme pages.
 * You should not call this function directly, use yo_register_metabox and yo_register_options.
 *
 */

function yo_register_fields($fields)
{
	global $yo_multiple_fields;

	// TODO: Add automatic check in the Fields class
	$multiple_fields = array('checkbox_list', 'file', 'image', 'plupload_image');

	foreach ($fields as $field)
	{
		$multiple = in_array($field['type'], $multiple_fields);

		if (isset($field['clone']) || isset($field['multiple']) || $multiple)
			$yo_multiple_fields[] = $field['id'];
	}
}


/**
 *------------------------------------------------------------------------------------------------
 * Register Meta Boxes
 *------------------------------------------------------------------------------------------------
 *
 * Helper function to register the defined $metaboxes for admin pages.
 *
 */

function yo_register_metabox($metaboxes)
{
	if (class_exists('YO_Metabox'))
	{
	    foreach ($metaboxes as $metabox)
	        new YO_Metabox($metabox);
	}
	else
	{
		foreach ($metaboxes as $metabox)
	    	yo_register_fields($metabox['fields']);
	}
}


/**
 *------------------------------------------------------------------------------------------------
 * Register Theme Options
 *------------------------------------------------------------------------------------------------
 *
 * Helper function to register the defined $options for admin pages.
 *
 */

function yo_register_options($options)
{
	if (class_exists('YO_Options'))
	{
	    new YO_Options($options);
	}
	else
	{
		foreach ($options as $option)
	    	yo_register_fields($option['fields']);
	}
}


/**
 *------------------------------------------------------------------------------------------------
 * Theme Helpers
 *------------------------------------------------------------------------------------------------
 *
 * A set of functions to easily display the data inside theme pages.
 *
 */

function get_field($field_name, $post_id = false) 
{ 
	global $post; 
	
	// Use the current post ID if not set
	if( ! $post_id) 
		$post_id = $post->ID; 
	
	// allow for option == options
	if($post_id == "option")
		$post_id = "options";

	// Set the current scope between post custom fields and theme options
	$prefix = 'yo_cache_';
	$scope = (is_numeric($post_id)) ? 'meta_'.$post_id : 'options';

	// Return the cache if already set
	$cache = wp_cache_get($prefix.$scope); 
	if ($cache)
		return (isset($cache[$field_name])) ? yo_check_multiple_field($field_name, $cache[$field_name]) : false;
		// return (isset($cache[$field_name])) ? unserialize_field($field_name, $cache[$field_name]) : false;

	// No cache available so get the fresh data
	$data = array();
	$data = (is_numeric($post_id)) ? get_post_custom($post_id) : get_option(THEME_OPTIONS);

	// Set the new value and update the cache
	$value = '';
	if ( ! empty($data))
	{
		wp_cache_set($prefix.$scope, $data); 
		$value = (isset($data[$field_name])) ? yo_check_multiple_field($field_name, $data[$field_name]) : false;
	}

	return $value;
}

function the_field($field_name, $post_id = false)
{
	$value = get_field($field_name, $post_id);
	
	if (is_array($value))
		$value = @implode(', ',$value);

	$value = do_shortcode($value);

	echo $value;
}

function yo_get_option($field_name)
{
	$value = get_field($field_name, 'option');
	return $value;
}

function the_option($field_name)
{
	$value = yo_get_option($field_name);
	
	if (is_array($value))
		$value = @implode(', ',$value);

	echo $value;
}

// Check if it's a multiple or cloneable field.
// Returns a multiple array to be used in a loop, or the single value for unique items.
function yo_check_multiple_field($field_name, $value)
{
	global $yo_multiple_fields;

	// First thing, check if it's a multiple or cloneable field
	if (in_array($field_name, $yo_multiple_fields))
		return (yo_is_serialized($value[0])) ? unserialize($value[0]) : $value;

	// Otherwise it's a single field...

	// Unserialize the array if necessary
	if (isset($value[0]) && yo_is_serialized($value[0]))
		$value = unserialize($value[0]);

	// Check if it's an array (group field) or a single item
	if (is_array($value))
		$value = (count($value) > 1) ? $value : $value[0];

	return $value;
}


// Echo the image URL
// TODO: remove $post_id and get it from globals in get_the_image()
function the_image($post_id, $size = 'full')
{
	echo get_the_image($post_id, $size);
}

// Get the image URL value
function get_the_image($post_id, $size = 'full')
{
	$image = wp_get_attachment_image_src($post_id, $size);
	$image = (isset($image[0])) ? $image[0] : '';
	return $image;
}