<?php
/*
Title		: Yow Framework - Generator of Fields, Meta Boxes, Theme Options, Shortcodes
Description	: Yow is an extendable WordPress admin suite that helps generating form fields, meta boxes, theme options and shortcodes.
Version		: 1.0.0
Author		: Giordano Piazza
Author URI	: http://giordanopiazza.com
License		: GPLv2+
Credits		: Meta Box Script - http://www.deluxeblogtips.com/meta-box/
			  Slightly Modified Options Framework - https://github.com/sy4mil/Options-Framework
			  Thematic Options Panel - http://wptheming.com/2010/11/thematic-options-panel-v2/
		 	  Woo Themes - http://woothemes.com/
		 	  Option Tree - http://wordpress.org/extend/plugins/option-tree/
*/

// Prevent loading this file directly
if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

// Metabox Generator Class
if ( ! class_exists('YO_Metabox'))
{
	class YO_Metabox
	{
		/**
		 * Meta box information
		 */
		var $metabox;

		/**
		 * Fields information
		 */
		var $fields;

		/**
		 * Fields HTML output
		 */
		var $fields_output;


		/**
		 *------------------------------------------------------------------------------------------------
		 * Create meta box with given data
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function __construct($metabox)
		{
			// Run script only in admin area
			if ( ! is_admin())
				return;
			
			$this->metabox = $metabox;
			$this->fields = &$this->metabox['fields'];
			$this->fields_output = new YO_Fields($this->fields);

			// Add meta box
			foreach ($this->metabox['pages'] as $page)
				add_action( "add_meta_boxes_{$page}", array(&$this, 'add_meta_boxes'));

			// Save post meta
			add_action('save_post', array($this, 'save_post'));
		}


		/**
		 * Add meta boxes for selected post types
		 *
		 * @return void
		 */
		public function add_meta_boxes()
		{
			foreach ($this->metabox['pages'] as $page)
			{
				add_meta_box(
					$this->metabox['id'],
					$this->metabox['title'],
					array( $this, 'render_metabox' ),
					$page,
					$this->metabox['context'],
					$this->metabox['priority']
				);
			}
		}


		/**
		 * Callback function to show fields in meta box
		 *
		 * @return void
		 */
		public function render_metabox()
		{
			global $post;

			$saved = self::has_been_saved($post->ID, $this->fields);

			// Using the 'nonce' for security
			wp_nonce_field( "yo-save-{$this->metabox['id']}", "nonce_{$this->metabox['id']}" );

			// Set the values for each field
			$values = array();
			foreach ($this->fields as $field)
				$values[$field['id']] = YO_Fields::apply_field_class_filters($field, 'meta', $post->ID, $saved);

			// Generate the HTML for the fields
			$this->fields_output = $this->fields_output->generate($values);

			// Allow users to add custom code before meta box content
			// 1st action applies to all meta boxes
			// 2nd action applies to only current meta box
			do_action( 'yo_before_metabox' );
			do_action( "yo_before_{$this->metabox['id']}" );

			// Output the fields
			echo '<div class="yo-metabox yo-container">';
			echo $this->fields_output;
			echo '</div>';

			// Allow users to add custom code after meta box content
			// 1st action applies to all meta boxes
			// 2nd action applies to only current meta box
			do_action( 'yo_after_metabox' );
			do_action( "yo_after_{$this->metabox['id']}" );
		}


		/**
		 * Metabox meta retrieval
		 *
		 * @param mixed $meta
		 * @param int	$post_id
		 * @param array $field
		 * @param bool  $saved
		 *
		 * @return mixed
		 */
		static function meta($post_id, $saved, $field)
		{
			$meta = get_post_meta($post_id, $field['id'], ! isset($field['multiple']));

			// Use $field['std'] only when the meta box hasn't been saved (i.e. the first time we run)
			if (isset($field['std']))
				$meta = (! $saved && '' === $meta || array() === $meta) ? $field['std'] : $meta;

			// Escape attributes for non-wysiwyg fields
			if ($field['type'] !== 'wysiwyg' && $field['type'] !== 'group')
				$meta = is_array( $meta ) ? array_map( 'esc_attr', $meta ) : esc_attr( $meta );

			// echo '<br>'.$field['id'];
			// echo '<pre>';
			// print_r($meta);
			// echo '</pre>';

			return $meta;
		}


		/**************************************************
			SAVE META BOX
		**************************************************/

		/**
		 * Save data from meta box
		 *
		 * @param int $post_id Post ID
		 *
		 * @return int|void
		 */
		function save_post($post_id)
		{
			// echo '<pre>';
			// print_r($_POST);
			// echo '</pre>';

			global $post_type;
			$post_type_object = get_post_type_object( $post_type );

			// Check whether:
			// - the post is autosaved
			// - the post is a revision
			// - current post type is supported
			// - user has proper capability
			if (
				( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
				|| ( ! isset($_POST['post_ID']) || $post_id != $_POST['post_ID'])
				|| ( ! in_array($post_type, $this->metabox['pages']))
				|| ( ! current_user_can($post_type_object->cap->edit_post, $post_id))
				)
			{
				return $post_id;
			}

			// Verify nonce
			check_admin_referer( "yo-save-{$this->metabox['id']}", "nonce_{$this->metabox['id']}" );

			foreach ($this->fields as $field)
			{
				$name = $field['id'];

				$old  = get_post_meta($post_id, $name, ! isset($field['multiple']));
				$new  = isset($_POST[$name]) ? $_POST[$name] : (isset($field['multiple']) ? array() : '');

				// echo 'saving...'.$field['name'].'<br>';
				// echo '<pre>';
				// print_r($old);
				// echo '</pre>';
				// echo '--------------<br>';
				// echo '<pre>';
				// print_r($new);
				// echo '</pre>';

				// echo 'field: ' . $field['id'];
				// echo ' /// value: ' . $_POST[$name];
				// echo '<pre>';
				// print_r($_POST);
				// echo '</pre>';


				// Allow field class change the value
				$new = YO_Fields::apply_field_class_filters($field, 'value', $new, $old, $post_id);

				// echo '<pre>';
				// print_r($new);
				// echo '</pre>';

				// Use filter to change field value
				// 1st filter applies to all fields with the same type
				// 2nd filter applies to current field only
				$new = apply_filters( "yo_{$field['type']}_value", $new, $field, $old );
				$new = apply_filters( "yo_{$name}_value", $new, $field, $old );

				// Call defined method to save meta value, if there's no methods, call common one
				YO_Fields::do_field_class_actions( $field, 'save', $new, $old, $post_id );
			}
		}


		/**
		 * Common functions for saving field
		 *
		 * @param mixed $new
		 * @param mixed $old
		 * @param int $post_id
		 * @param array $field
		 *
		 * @return void
		 */
		static function save($new, $old, $post_id, $field)
		{
			// if ($field['type'] == 'group')
			// {
			// 	echo '<pre>';
			// 	print_r($new);
			// 	echo '</pre>';
			// }

			// echo '-----------------------------------<br>';
			// echo $field['id'] .' /// '. $field['multiple'].'<br>';
			// echo '<pre>';
			// print_r($new);
			// echo '</pre>';

			$name = $field['id'];

			delete_post_meta($post_id, $name);
			if ('' === $new || array() === $new)
				return;

			if (isset($field['multiple']))
			{
				echo $field['id'].': multiple field<br>';
				foreach ($new as $add_new)
					add_post_meta($post_id, $name, $add_new, false);
			}
			else
			{
				update_post_meta($post_id, $name, $new);
			}
		}


		/**
		 * Check if meta box has been saved
		 * This helps saving empty value in meta fields (for text box, check box, etc.)
		 *
		 * @param int   $post_id
		 * @param array $fields
		 *
		 * @return bool
		 */
		static function has_been_saved($post_id, $fields)
		{
			$saved = false;
			foreach ($fields as $field)
			{
				if (get_post_meta($post_id, $field['id'], ! isset($field['multiple'])))
				{
					$saved = true;
					break;
				}
			}
			return $saved;
		}
	}
}