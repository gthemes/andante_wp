<?php
/*
Title       : Yow Framework - Generator of Fields, Meta Boxes, Theme Options, Shortcodes
Description : Yow is an extendable WordPress admin suite that helps generating form fields, meta boxes, theme options and shortcodes.
Version     : 1.0.0
Author      : Giordano Piazza
Author URI  : http://giordanopiazza.com
License     : GPLv2+
Credits     : Meta Box Script - http://www.deluxeblogtips.com/meta-box/
              Slightly Modified Options Framework - https://github.com/sy4mil/Options-Framework
              Thematic Options Panel - http://wptheming.com/2010/11/thematic-options-panel-v2/
              Woo Themes - http://woothemes.com/
              Option Tree - http://wordpress.org/extend/plugins/option-tree/
*/


// Prevent direct access to this file
if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }



/*
 *------------------------------------------------------------------------------------------------
 * Add a custom button to TinyMCE editor
 *------------------------------------------------------------------------------------------------
 *
 *
 */

add_action('admin_head', 'yo_add_tinymce');
function yo_add_tinymce() {
    global $typenow;

    // only on Post Type: post and page
    if( ! in_array( $typenow, array( 'post', 'page' ) ) )
        return ;

    add_filter( 'mce_external_plugins', 'yo_add_tinymce_plugin' );
    // Add to line 1 form WP TinyMCE
    add_filter( 'mce_buttons', 'yo_add_tinymce_button' );
}

// inlcude the js for tinymce
function yo_add_tinymce_plugin( $plugin_array ) {

    $plugin_array['yo_sc_button'] = YO_JS_URL.'tinymce/editor_plugin.js';
    // Print all plugin js path
    // var_dump( $plugin_array );
    return $plugin_array;
}

// Add the button key for address via JS
function yo_add_tinymce_button( $buttons ) {

    array_push( $buttons, 'yo_sc_button_key' );
    // Print all buttons
    // var_dump( $buttons );
    return $buttons;
}

// function add_shortcodes_button()
// {
//    if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages'))
//      return;

//    if (get_user_option('rich_editing') == 'true')
//    {
//      // add_filter('mce_external_plugins', 'add_shortcodes_tinymce_plugin');
//      add_filter('mce_buttons', 'register_shortcodes_button');
//    }
// }

// add_action('init', 'add_shortcodes_button');


// function register_shortcodes_button($buttons)
// {
//   array_push($buttons, "|", "yo_sc_button");
//   return $buttons;
// }

// function add_shortcodes_tinymce_plugin($plugin_array)
// {
//   $plugin_array['yo_sc_button'] = YO_JS_URL.'tinymce/editor_plugin.js';
//   return $plugin_array;
// }


// Refresh TinyMCE (only for development)
// function my_refresh_mce($ver) {
//   $ver += 3;
//   return $ver;
// }

// add_filter( 'tiny_mce_version', 'my_refresh_mce');


// Set the shortcodes editor url
function yo_shortcodes_init()
{
  // echo get_admin_url();
  // wp_enqueue_script('yo-shortcodes', YO_URL.'/assets/js/tinymce/editor_plugin.js', array('jquery'), '1.0.0', true);
  wp_localize_script('jquery', 'yo_shortcodes', array('editor_page' => get_admin_url().'options-general.php?page=yo-shortcodes-editor',
                                                      'assets'      => YO_URL.'assets/'));
}

add_action('admin_init', 'yo_shortcodes_init');


function yo_after_wp_tiny_mce()
{
  echo '<style type="text/css">
        .mce-yo-sc-button { background: none !important; border: 0 !important; }
        .mce-yo-sc-button .mce-caret { /*right: -7px !important;*/ /*display: none !important;*/ }
        .mce-yo-sc-button button { padding-right: 18px !important; }
        </style>';

  printf('<script type="text/javascript" src="%s"></script>',  YO_URL.'/assets/js/tinymce/editor_plugin.js');
}

add_action('after_wp_tiny_mce', 'yo_after_wp_tiny_mce');





if (is_admin())
{
  // Define the fields for the shortcodes editor
  $yo_shortcodes['video'] = array(
        'shortcode' => '[yo_video type="{{type}}" src="{{src}}" width="{{width}}" height="{{height}}"]',
        'fields' => array(
            array(
                'heading'   => __('Video embed', 'theme_admin'),
                'name'      => __('Video Source', 'theme_admin'),
                'id'        => 'type',
                'type'      => 'select',
                'options'   => array(
                    'youtube'  => 'Youtube',
                    'vimeo'    => 'Vimeo',
                    'ustream'  => 'Ustream',
                    'custom'   => 'Custom'
                ),
                'std'       => 'youtube'
            ), // column width
            array(
              'name'      => __('URL', 'theme_admin'),
              'id'        => 'src',
              'type'      => 'text',
              'desc'      => __("Insert the video URL, the same you would copy and paste to share. If 'Custom' is selected it's possible to embed any HTML code using this field.", 'theme_admin')
            ),
            array(
              'name'      => __('Width', 'theme_admin'),
              'id'        => 'width',
              'type'      => 'text',
              'desc'      => __('Insert the video width. (Optional)', 'theme_admin')
            ),
            array(
              'name'      => __('Height', 'theme_admin'),
              'id'        => 'height',
              'type'      => 'text',
              'desc'      => __('Insert the video height. (Optional)', 'theme_admin')
            )
        ),
  );

  $yo_shortcodes['maps'] = array(
        'shortcode' => '[yo_maps src="{{src}}" width="{{width}}" height="{{height}}"]',
        'fields' => array(
            array(
              'heading'   => __('Maps embed', 'theme_admin'),
              'name'      => __('URL', 'theme_admin'),
              'id'        => 'src',
              'type'      => 'text',
              'desc'      => __("Insert the maps URL, the same you would copy and paste to share.", 'theme_admin')
            ),
            array(
              'name'      => __('Width', 'theme_admin'),
              'id'        => 'width',
              'type'      => 'text',
              'desc'      => __('Insert the video width. (Optional)', 'theme_admin')
            ),
            array(
              'name'      => __('Height', 'theme_admin'),
              'id'        => 'height',
              'type'      => 'text',
              'desc'      => __('Insert the video height. (Optional)', 'theme_admin')
            )
        ),
  );

  $yo_shortcodes['hero'] = array(
      'shortcode' => '[yo_hero text="{{text}}"]',
      'fields' => array(
          array(
              'heading'   => __('Hero heading text', 'theme_admin'),
              'name'      => __('Text', 'theme_admin'),
              'id'        => 'text',
              'type'      => 'text',
              'desc'      => __('Insert some text that will be shown at a big format.', 'theme_admin')
          )
        )
  );

  $yo_shortcodes['button'] = array(
      'shortcode' => '[yo_button href="{{href}}" target="{{target}}" type="{{type}}"]{{content}}[/yo_button]',
      'fields' => array(
          array(
            'heading'   => __('Button', 'theme_admin'),
            'name'      => __('Label', 'theme_admin'),
            'id'        => 'content',
            'type'      => 'text',
            'desc'      => __('Insert the button label.', 'theme_admin')
          ),
          array(
              'name'      => __('Type', 'theme_admin'),
              'id'        => 'type',
              'type'      => 'select',
              'options'   => array(
                  ''        => 'Normal',
                  'full'    => 'Full width',
                  'big'     => 'Big',
                  'small'   => 'Small',
                  'mini'    => 'Mini'
              ),
              'std'       => ''
          ),
          array(
            'name'      => __('URL', 'theme_admin'),
            'id'        => 'href',
            'type'      => 'text',
            'desc'      => __("The URL address link.", 'theme_admin')
          ),
          array(
              'name'      => __('Target', 'theme_admin'),
              'id'        => 'target',
              'type'      => 'select',
              'options'   => array(
                  '_blank'  => 'New window',
                  '_self'   => 'Same window',
                  '_top'    => 'Top',
              ),
              'std' => '_blank'
          )
      ),
  );

  $yo_shortcodes['message'] = array(
        'shortcode' => '[yo_message type="{{type}}"]{{content}}[/yo_message]',
        'fields' => array(
            array(
                'heading'   => __('Message box', 'theme_admin'),
                'name'      => __('Type', 'theme_admin'),
                'id'        => 'type',
                'type'      => 'select',
                'options'   => array(
                    'info'    => 'Info',
                    'success' => 'Success',
                    'warning' => 'Warning',
                    'error'   => 'Error'
                ),
                'std'       => 'info'
            ),
            array(
              'name'      => __('Message text', 'theme_admin'),
              'id'        => 'content',
              'type'      => 'text',
              'desc'      => __("The text to be displayed in the message box.", 'theme_admin')
            ),
        ),
  );

  $yo_shortcodes['columns'] = array(
        'shortcode' => '{{clone:columns_group}}[yo_column title="{{title}}" span="{{span}}"]{{content}}[/yo_column]{{/clone}}',
        'fields' => array(
            array(
              'heading'   => __('Columns', 'theme_admin'),
              'name'      => __('Add Columns', 'theme_admin'),
              'desc'      => __("You can make any combination of columns following these rules: if you are editing a page using the 'Fullwidth' template the max columns span is '12' (example: 3+9, or 2+4+6 etc...); in the 'Portfolio' articles or the page template 'Sidebar' the max columns span is '8' (example: 6+2, or 3+2+3 etc...).", 'theme_admin'),
              'id'     => 'columns_group',
              'type'   => 'group',
              'clone'  => true,
              'sortable'  => true,
              'fields' => array(
                array(
                    'name'      => __('Column width', 'theme_admin'),
                    'id'        => 'span',
                    'type'      => 'select',
                    'options'   => array(
                        '1'  => '1/12',
                        '2'  => '2/12',
                        '3'  => '3/12',
                        '4'  => '4/12',
                        '5'  => '5/12',
                        '6'  => '6/12',
                        '7'  => '7/12',
                        '8'  => '8/12',
                        '9'  => '9/12',
                        '10' => '10/12',
                        '11' => '11/12',
                        '12' => '12/12'
                      ),
                    'std'       => '12'
                ), // column width
                array(
                  'name'      => __('Title', 'theme_admin'),
                  'id'        => 'title',
                  'type'      => 'text',
                ),
                array(
                  'name'      => __('Content', 'theme_admin'),
                  'id'        => 'content',
                  'type'      => 'textarea',
                )
              ) // group fields
            ) // group 
            
          ), // shortcode fields
  );
  
  $yo_shortcodes['tabs'] = array(
        'shortcode' => '[yo_tabs]{{clone:tabs_group}}[yo_tab id="{{id}}" title="{{title}}"]{{content}}[/yo_tab]{{/clone}}[/yo_tabs]',
        'fields' => array(
            array(
              'heading'   => __('Tabs', 'theme_admin'),
              'name'      => __('Add Tabs', 'theme_admin'),
              'desc'      => __("The 'ID' must be lowercase only using letters, dashes and underscores. The 'Title' will be used in the tab navigation buttons. The 'Content' can be simple text and HTML.", 'theme_admin'),
              'id'        => 'tabs_group',
              'type'      => 'group',
              'clone'     => true,
              'sortable'  => true,
              'fields'    => array(
                array(
                  'name'      => __('ID', 'theme_admin'),
                  'id'        => 'id',
                  'type'      => 'text',
                ),
                array(
                  'name'      => __('Title', 'theme_admin'),
                  'id'        => 'title',
                  'type'      => 'text',
                ),
                array(
                  'name'      => __('Content', 'theme_admin'),
                  'id'        => 'content',
                  'type'      => 'textarea',
                )
              ) // group fields
            ) // group 
            
          ), // shortcode fields
  );

  $yo_shortcodes['accordion'] = array(
        'shortcode' => '[yo_accordions]{{clone:accordion_group}}[yo_accordion title="{{title}}"]{{content}}[/yo_accordion]{{/clone}}[/yo_accordions]',
        'fields' => array(
            array(
              'heading'   => __('Accordion', 'theme_admin'),
              'name'      => __('Add Pane', 'theme_admin'),
              'desc'      => __("The 'Title' will be used in the accordion navigation buttons. The 'Content' can be simple text and HTML.", 'theme_admin'),
              'id'        => 'accordion_group',
              'type'      => 'group',
              'clone'     => true,
              'sortable'  => true,
              'fields'    => array(
                array(
                  'name'      => __('Title', 'theme_admin'),
                  'id'        => 'title',
                  'type'      => 'text',
                ),
                array(
                  'name'      => __('Content', 'theme_admin'),
                  'id'        => 'content',
                  'type'      => 'textarea',
                )
              ) // group fields
            ) // group 
            
          ), // shortcode fields
  );
  
  $yo_shortcodes['slider'] = array(
        'shortcode' => '[yo_slider]{{clone:slider_group}}[yo_slide title="{{title}}" image="{{image}}" link="{{link}}" target="{{target}}"]{{/clone}}[/yo_slider]',
        'fields' => array(
            array(
              'heading'   => __('Slider', 'theme_admin'),
              'name'      => __('Add slides', 'theme_admin'),
              'desc'      => __("Add images URLs to the slider, along with their link and target.", 'theme_admin'),
              'id'        => 'slider_group',
              'type'      => 'group',
              'clone'     => true,
              'sortable'  => true,
              'fields'    => array(
                array(
                  'name'      => __('Title', 'theme_admin'),
                  'id'        => 'title',
                  'type'      => 'text',
                ),
                array(
                  'name'      => __('Image URL', 'theme_admin'),
                  'id'        => 'image',
                  'type'      => 'text',
                ),
                array(
                  'name'      => __('Link', 'theme_admin'),
                  'id'        => 'link',
                  'type'      => 'text',
                ),
                array(
                    'name'      => __('Target', 'theme_admin'),
                    'id'        => 'target',
                    'type'      => 'select',
                    'std'       => '_self',
                    'options'   => array(
                        '_self'        => __('_self', 'theme_admin'),
                        '_blank'       => __('_blank', 'theme_admin'),
                        '_top'         => __('_top', 'theme_admin'),
                    ),
                ),
              ) // group fields
            ) // group 
            
          ), // shortcode fields
  );

  $yo_shortcodes['list'] = array(
        'shortcode' => '[yo_ul]{{clone:list_group}}[yo_li icon="{{icon}}" icon_color="{{icon_color}}"]{{content}}[/yo_li]{{/clone}}[/yo_ul]',
        'fields' => array(
            array(
              'heading'   => __('List', 'theme_admin'),
              'name'      => __('Add Pane', 'theme_admin'),
              'desc'      => __("The 'Title' will be used in the accordion navigation buttons. The 'Content' can be simple text and HTML.", 'theme_admin'),
              'id'        => 'list_group',
              'type'      => 'group',
              'clone'     => true,
              'sortable'  => true,
              'fields'    => array(
                array(
                  'name'      => __('Text', 'theme_admin'),
                  'id'        => 'content',
                  'type'      => 'text',
                ),
                array(
                  'name'      => __('Icon Color', 'theme_admin'),
                  'id'        => 'icon_color',
                  'type'      => 'select',
                  'options'   => array(
                      'dark'  => __('White', 'theme_admin'),
                      'light' => __('Black', 'theme_admin')),
                  'std'       => 'white'
                ),
                array(
                  'name'      => __('Icon Type', 'theme_admin'),
                  'id'        => 'icon',
                  'type'      => 'select',
                  'options'   => array(
                      ''          => __('No icon', 'theme_admin'),
                      'add'       => __('Add', 'theme_admin'),
                      'arrow1e'   => __('Arrow 1 E', 'theme_admin'),
                      'arrow1se'  => __('Arrow 1 SE', 'theme_admin'),
                      'arrow2e'   => __('Arrow 2 E', 'theme_admin'),
                      'arrow3e'   => __('Arrow 3 E', 'theme_admin'),
                      'check'     => __('Check', 'theme_admin'),
                      'close'     => __('Close', 'theme_admin'),
                      'forward'   => __('Forward', 'theme_admin'),
                      'stop'      => __('Stop', 'theme_admin'),
                      'subtract'  => __('Subtract', 'theme_admin'),
                  'std'       => '')
                ),
              ) // group fields
            ) // group 
            
          ), // shortcode fields
    );

  $yo_shortcodes['quote'] = array(
        'shortcode' => '[yo_quote author="{{author}}"]{{content}}[/yo_quote]',
        'fields' => array(
            array(
                  'heading'   => __('Blockquote', 'theme_admin'),
                  'name'      => __('Author / Cite', 'theme_admin'),
                  'id'        => 'author',
                  'type'      => 'text',
            ),
            array(
              'name'      => __('Content', 'theme_admin'),
              'id'        => 'content',
              'type'      => 'textarea',
            )
          ), // shortcode fields
  );

  add_action('admin_menu', 'register_shortcodes_page');

  function register_shortcodes_page()
  {
    global $_registered_pages; 

    // Get the name of the hook for this plugin  
    // We use "options-general.php" as the parent as we want our page to appear under "options-general.php?page=my-plugin-hidden-page"  
    $hookname = get_plugin_page_hookname('yo-shortcodes-editor', 'options-general.php');

    // Add the callback via the action on $hookname, so the callback function is called when the page "options-general.php?page=my-plugin-hidden-page" is loaded  
    if (!empty($hookname)) {  
        add_action($hookname, 'yo_shortcodes_editor');  
    }  
  
    // Add this page to the registered pages  
    $_registered_pages[$hookname] = true;  
  }

  function yo_shortcode_fields($shortcode = false)
  {
    global $yo_shortcodes;
    if (isset($yo_shortcodes[$shortcode]['fields']))
    {
      $shortcode_fields = new YO_Fields($yo_shortcodes[$shortcode]['fields']);
      return $shortcode_fields->generate();
    }
  }

  function yo_shortcode_template($shortcode = false)
  {
    global $yo_shortcodes;
    if (isset($yo_shortcodes[$shortcode]['shortcode']))
    {
      return $yo_shortcodes[$shortcode]['shortcode'];
    }
  }

  function yo_shortcodes_editor()
  {
    $shortcode = isset($_GET['shortcode']) ? $_GET['shortcode'] : '';

    // Set some styles for the page
    echo '<style>';
    echo 'html { background:#f5f5f5; }
          #adminmenuwrap, #adminmenuback, #wpadminbar { display:none; }
          html.wp-toolbar, body.admin-bar #wpcontent { margin:0 !important; padding-top:0; }
          #wpbody-content { padding-bottom:0; }
           
          #footer {display:none;} 
          .yo-container { padding:20px; }
          /*#yo-shortcode-bar { background:#ececec; padding:15px; text-align:right; }*/
          #yo-insert-shortcode { width: 100%; color: #fff; text-align: center; }
          .updated { display:none; }';
    echo '</style>';

    // Open the main container and form
    echo '<form method="post" id="yo-sc-form"><div id="yo-shortcodes" class="yo-container">';

    // Show the fields
    echo yo_shortcode_fields($shortcode);

    // Set the shortcode template in a hidden field
    echo '<div id="yo-shortcode-template" class="hidden">'.yo_shortcode_template($shortcode).'</div>';

    // Display the 'Insert Shortcode' button
    echo '<div id="yo-shortcode-bar"><a id="yo-insert-shortcode" class="button button-primary">'.__('Insert the Shortcode', 'theme_admin').'</a></div>';

    // Close the main container and form
    echo '</div></form>';
  }

} // End admin check