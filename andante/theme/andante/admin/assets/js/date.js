/**
 * Update date picker element
 * Used for static & dynamic added elements (when clone)
 */
function yo_update_date_picker() {
	var $ = jQuery;

	$('.yo-date input').each(function() {
		var $this = $(this),
			format = $this.attr('rel');

		$this.removeClass('hasDatepicker').attr('id', '').datepicker( {
			showButtonPanel: true,
			dateFormat: format
		});
	});
}

jQuery(document).ready(function($) {
	yo_update_date_picker();
});