/*-----------------------------------------------------------------------------------*/
/* WooFramework Media Library-driven AJAX File Uploader Module
/* JavaScript Functions (2010-11-05)
/*
/* The code below is designed to work as a part of the WooFramework Media Library-driven
/* AJAX File Uploader Module. It is included only on screens where this module is used.
/*
/* Used with (very) slight modifications for Options Framework.
/*-----------------------------------------------------------------------------------*/
(function ($) {

    optionsframeworkMLU = {

        /*-----------------------------------------------------------------------------------*/
        /* Remove file when the "remove" button is clicked.
        /*-----------------------------------------------------------------------------------*/

        removeFile: function () {

            // $('.mlu_remove_button').live('click', function(event) { 
            $('.yo-remove-media').live('click', function (e) {
                var clickedObject = $(this);
                var theparent = $(this).parent().parent();
                var theID = $(this).attr('data-remove');
                // var image_to_remove = $('.image_' + theID, theparent);
                var image_to_remove = $('.yo-media-thumb *', theparent);
                // console.log($(this).parent('.yo-input'));
                var button_to_hide = $('.reset_' + theID, theparent);
                image_to_remove.fadeOut(500, function () {
                    $(this).remove();
                });
                button_to_hide.fadeOut();
                $('input[type="text"]', theparent).val('');
                // console.log($('input.hide', theparent));

                return false;
            });

            // Hide the delete button on the first row 
            $('a.delete-inline', "#option-1").hide();

        },
        // End removeFile


        /*-----------------------------------------------------------------------------------*/
        /* Replace the default file upload field with a customised version.
        /*-----------------------------------------------------------------------------------*/

        recreateFileField: function () {

            $('input.file').each(function () {
                var uploadbutton = '<input class="upload_file_button" type="button" value="Upload" />';
                $(this).wrap('<div class="file_wrap" />');
                $(this).addClass('file').css('opacity', 0); //set to invisible
                $(this).parent().append($('<div class="fake_file" />').append($('<input type="text" class="upload" />').attr('id', $(this).attr('id') + '_file')).val($(this).val()).append(uploadbutton));

                $(this).bind('change', function () {
                    $('#' + $(this).attr('id') + '_file').val($(this).val());
                });
                $(this).bind('mouseout', function () {
                    $('#' + $(this).attr('id') + '_file').val($(this).val());
                });
            });

        },
        // End recreateFileField


        /*-----------------------------------------------------------------------------------*/
        /* Use a custom function when working with the Media Uploads popup.
        /* Requires jQuery, Media Upload and Thickbox JavaScripts.
        /*-----------------------------------------------------------------------------------*/

        mediaUpload: function () {

            jQuery.noConflict();

            // $( '.yo-upload-media' ).removeAttr('style');
            var formfield, formID, btnContent = true,
                tbframe_interval, theparent, attachmentID;
            // On Click
            $('.yo-upload-media').live('click', function (e) {
                clickedObject = $(this);
                theparent = $(this).parent().parent();
                formfield = $(this).parent().prev('input').attr('id');

                function setID(e){
                    attachmentID = $(e.target).attr('id');
                    attachmentID = attachmentID.replace(/.*\[|\]/gi,'');
                }
                
                //Change "insert into post" to "Use this Button"
                tbframe_interval = setInterval(function () {
                    
                    jQuery('#TB_iframeContent').contents().find('tr.post_title,tr.image_alt,tr.post_excerpt,tr.image-size,tr.post_content,tr.url,tr.align,tr.submit>td>a.del-link').hide();

                    jQuery('#TB_iframeContent').contents().find('.savesend .button').val('Use This Image');
                                        
                    jQuery('#TB_iframeContent').contents().find('.savesend .button').off('click', setID);
                    jQuery('#TB_iframeContent').contents().find('.savesend .button').on('click', setID);

                    // Hide some stuff
                    if (jQuery('#TB_iframeContent').contents().find('tr.post_title').length) {
                        jQuery('#TB_iframeContent').contents().find('tr.image-size input[value="full"]').prop('checked', true);
                        jQuery('#TB_iframeContent').contents().find('tr.post_title,tr.image_alt,tr.post_excerpt,tr.image-size,tr.post_content,tr.url,tr.align,tr.submit>td>a.del-link').hide();
                    }

                    // Place the "Quick attach" button to the Media Gallery
                    // if (!jQuery('#TB_iframeContent').contents().find('.attach-quick-link').length) {
                    //     // toggle describe-toggle-off
                    //     jQuery('#TB_iframeContent').contents().find('.toggle.describe-toggle-on').before('<a class="attach-quick-link button" href="#" style="float:right;margin:7px;">Attach</a>');
                    //     jQuery('#TB_iframeContent').contents().find('.attach-quick-link').on('click', function(){
                    //         //jQuery('#TB_iframeContent').contents().find('.media-item .savesend input[type=submit], #insertonlybutton').trigger('click');
                    //         jQuery('input[type="submit"]', jQuery(this).parent()).trigger('click');
                    //         self.parent.tb_remove();
                    //     });
                    // }

                }, 2000); // Interval time

                // Display a custom title for each Thickbox popup.
                var yo_title = '';

                // if ($(this).parents('.yo-field').find('.yo-heading')) {
                //     yo_title = $(this).parents('.yo-field').find('.yo-heading').text();
                // } // End IF Statement

                tb_show(yo_title, 'media-upload.php?post_id=&TB_iframe=1');
                // tb_show(yo_title, 'media-upload.php?post_id=' + formID + '&TB_iframe=1');
                return false;
                // e.preventDefault();
            });

            window.original_send_to_editor = window.send_to_editor;
            window.send_to_editor = function (html) {
                // console.log(html);
                if (formfield) {

                    //clear interval for "Use this Button" so button text resets
                    clearInterval(tbframe_interval);
                    
                    // itemurl = $(html);
                    // console.log($(html));

                    // Get the attachment ID and URL
                    // itemurl = $(html).attr('href'); // Use the URL to the main image.
                    if ($(html).html(html).find('img').length > 0) {
                        // itemurl = theparent.parent().parent().find('img.pinkynail').attr('src');
                        itemurl = $(html).html(html).find('img').attr('src'); // Use the URL to the size selected.
                        // itemid = jQuery('img', html).attr('class').replace(/(.*?)wp-image-/, '');
                        // id = $classes.replace(/(.*?)wp-image-/, '');
                        // var test = jQuery('#TB_iframeContent').contents().find($(html));
                        // console.log(itemid);
                    } else {
                        // It's not an image. Get the URL to the file instead.
                        var htmlBits = html.split("'"); // jQuery seems to strip out XHTML when assigning the string to an object. Use alternate method.
                        itemurl = htmlBits[1]; // Use the URL to the file.
                        var itemtitle = htmlBits[2];

                        itemtitle = itemtitle.replace('>', '');
                        itemtitle = itemtitle.replace('</a>', '');
                    } // End IF Statement


                    var image = /(^.*\.jpg|jpeg|png|gif|ico*)/gi;
                    var document = /(^.*\.pdf|doc|docx|ppt|pptx|odt*)/gi;
                    var audio = /(^.*\.mp3|m4a|ogg|wav*)/gi;
                    var video = /(^.*\.mp4|m4v|mov|wmv|avi|mpg|ogv|3gp|3g2*)/gi;

                    if (itemurl.match(image)) {
                        // btnContent = '<a class="of-uploaded-image" href="'+itemurl+'"><img id="image_'+imgID+'" class="of-option-image" src="'+itemurl+'" alt="" /></a>';
                        btnContent = '<img class="image_' + attachmentID + '" src="' + itemurl + '" alt="" />';
                    } else {

                        // No output preview if it's not an image.
                        // btnContent = '';
                        // Standard generic output if it's not an image.
                        html = '<a href="' + itemurl + '" target="_blank" rel="external">View File</a>';
                        btnContent = 'The file is not an image: <a href="' + itemurl + '" target="_blank" rel="external">View File</a>';
                    }

                    // console.log(attachmentID);

                    // var image_to_remove = $('.yo-uploaded-image', theparent);
                    $('input[type="text"]', theparent).val(attachmentID);
                    $('.yo-media-thumb', theparent).html(btnContent).fadeIn();

                    // $('#' + formfield).val(itemurl);
                    // $('#' + formfield).next().next('div').slideDown().html(btnContent);
                    // $('#' + formfield).siblings('.yo-media-thumb').fadeIn().html(btnContent);
                    clickedObject.next('span').fadeIn(500);
                    tb_remove();

                } else {
                    window.original_send_to_editor(html);
                }

                // Clear the formfield value so the other media library popups can work as they are meant to. - 2010-11-11.
                formfield = '';
            }

        } // End mediaUpload
    }; // End optionsframeworkMLU Object // Don't remove this, or the sky will fall on your head.


    /*-----------------------------------------------------------------------------------*/
    /* Execute the above methods in the optionsframeworkMLU object.
    /*-----------------------------------------------------------------------------------*/

    $(document).ready(function () {

        optionsframeworkMLU.removeFile();
        optionsframeworkMLU.recreateFileField();
        optionsframeworkMLU.mediaUpload();

    });

})(jQuery);