(function($){

    window.wpActiveEditor = {};

    var doc = {
        ready: function(){
            media_library.init();
            // initialize only if our button is in the page
            // if($('.yo-media-manager').length > 0){
            //     media_library.init();
            // }
        }
    },
    media_library = {
        // the following 2 objects would be our backup containers
        // as we will be replacing the default media handlers
        media_insert: function(){},
        media_send_attachment: null,
        media_close_window: null,
        clicked_element: null,
        init: function(){
            // bind the button's click the browse_clicked handler
            // $('.yo-media-manager').click(media_library.browse_clicked);
            $('.yo-input').on('click', '.yo-upload-media', media_library.browse_clicked);
            $('.yo-input').on('click', '.yo-remove-media', media_library.remove_media);
        },
        browse_clicked: function(e){
            // cancel the event so we won't be navigated to href="#"
            e.preventDefault();
            
            // backup editor objects first
            media_library.media_send_attachment = wp.media.editor.send.attachment;
            media_library.media_close_window = wp.media.editor.remove;

            // override the objects with our own
            wp.media.editor.insert = media_library.media_insert;
            wp.media.editor.send.attachment = media_library.media_accept;
            wp.media.editor.remove = media_library.media_close;

            // Set the clicked element
            media_library.clicked_element = $(this);

            // open up the media manager window
            wp.media.editor.open();
        },
        media_accept: function(props, attachment){
            // this function is called when the media manager sends in media info
            // when the user clicks the "Insert into Post" button
            // this may be called multiple times (one for each selected file) 
            // you might be interested in the following:
            // alert(attachment.id); // this stands for the id of the media attachment passed
            // alert(attachment.url); // this is the url of the media attachment passed
            // for now let's log it the console
            // not you can do anything Javascript-ly possible here
            // console.log(props);
            // console.log(attachment);

            var data = {
                action: 'get_media',
                id: attachment.id,
                _ajax_nonce: yo_media['_ajax_nonce']
            };

            jQuery.post(ajaxurl, data, function(response) {
                $('input[type="text"]', media_library.clicked_element.parent().parent()).val(attachment.id);
                $('.yo-media-thumb', media_library.clicked_element.parent().parent()).html(response).fadeIn();
                media_library.clicked_element.next('span').fadeIn(500);
                media_library.media_close();
            });
        },
        media_close: function(id){
            // this function is called when the media manager wants to close
            // (either close button or after sending the selected items)

            // restore editor objects from backup
            wp.media.editor.insert = media_library.media_insert;
            wp.media.editor.send.attachment = media_library.media_send_attachment;
            wp.media.editor.remove = media_library.media_close_window;

            // nullify the backup objects to free up some memory
            // media_library.media_insert = null;
            media_library.media_send_attachment = null;
            media_library.media_close_window = null;

            // trigger the actual remove
            wp.media.editor.remove(id);
        },
        remove_media: function (e) {
            e.preventDefault();
            var scope = $(this).parents('.yo-media'),
                clone_parent = $(this).parents('.yo-clone');

            if (clone_parent.length > 0) {
                scope = clone_parent;
            }

            $('.yo-media-thumb img', scope).fadeOut(500, function () { $(this).remove(); });
            $(this).fadeOut();
            $('input[type="text"]', scope).val('');
        },
    };
    $(document).ready(doc.ready);
})(jQuery);