/*
 * Shortcode Editor
 * v1.0
*/
(function() {

	/**
	 * Get String between a Prefix String and a Suffix String
	 * Usage  var you = 'hello you guys'.between('hello ',' guys');
	 * you = 'you';
	 */
	String.prototype.between = function(prefix, suffix) {
	  s = this;
	  var i = s.indexOf(prefix);
	  if (i >= 0) {
	    s = s.substring(i + prefix.length);
	  }
	  else {
	    return '';
	  }
	  if (suffix) {
	    i = s.indexOf(suffix);
	    if (i >= 0) {
	      s = s.substring(0, i);
	    }
	    else {
	      return '';
	    }
	  }
	  return s;
	}

    tinymce.create('tinymce.plugins.yoShortcodes', {
        init: function (ed, url)
		{
			var yo = this;

			ed.addCommand("yoEditor", function (a, params)
			{
				var shortcode = params.identifier;
					// active = params.active;

				// Load thickbox
				tb_show("Insert Shortcode", yo_shortcodes.editor_page + "&post_id=&shortcode=" + shortcode + /*"&width=" + 650+ "&height=" + 550 +*/ "&TB_iframe=1");

				// Wait for the iframe to load
				jQuery('#TB_iframeContent').load(function()
				{
					// Resize the popup
					yo.tb_resize();
					jQuery(window).resize(function() { yo.tb_resize(); });

					jQuery("#TB_iframeContent").contents().find('#yo-insert-shortcode').click(function()
					{
						var shortcode_template = jQuery("#TB_iframeContent").contents().find('#yo-shortcode-template').text();
			    		
			    		// Clone groups
			    		// Fill in the gaps eg {{param}}
			    		// TODO: Change 'input, textarea...' with the serialized form data
			    		jQuery("#TB_iframeContent").contents().find('[data-clone]').each(function()
			    		{
			    			// Get the clone group id and the template to use for the clones
			    			var clone_group = jQuery(this).attr('data-clone'),
			    				clone_template = shortcode_template.between('{{clone:'+clone_group+'}}','{{/clone}}'),
			    				form_fields = 'input, textarea, select, radio, checkbox',
			    				output = '';

			    			if (clone_template)
			    			{
			    				// Get the clones
			    				jQuery("#TB_iframeContent").contents().find('.'+clone_group+' .yo-clone').each(function()
			    				{
			    					var clone = clone_template;

				    				// Get the fields of each clone
				    				jQuery(form_fields, jQuery(this)).each(function(){
				    					var input = jQuery(this),
				    						id = input.attr('id').replace(clone_group+'_', ''),
						    				re = new RegExp("{{"+id+"}}","g");
						    			clone = clone.replace(re, input.val());				    					
				    				});
				    				output += clone;
			    				});
			    			}

			    			// Remove the loop tags from the shortcode
			    			shortcode_template = shortcode_template.replace('{{clone:'+clone_group+'}}'+clone_template+'{{/clone}}', output);
			    		});// clone group
						
			    		// Single fields
			    		// Fill in the gaps eg {{param}}
			    		// TODO: Change 'input, textarea...' with the serialized form data
						jQuery("#TB_iframeContent").contents().find('.yo-input input, .yo-input textarea, .yo-input select').each(function() {
			    			var input = jQuery(this),
			    				id = input.attr('id'),
			    				re = new RegExp("{{"+id+"}}","g");
			    			shortcode_template = shortcode_template.replace(re, input.val());
			    		});

						// Remove any {{/clone}}
			    		// shortcode_template = shortcode_template.replace('{{clone:'+clone_group+'}}'+clone_template+'{{/clone}}', output);

						if(window.tinyMCE)
						{
							window.tinyMCE.activeEditor.execCommand("mceInsertContent", false, shortcode_template);
	                		tb_remove();
	                	}
					});
				});
			});
		},
        createControl: function (btn, e)
		{
			if (btn == "yo_sc_button")
			{
				var a = this;
				
				var btn = e.createSplitButton('yo_sc_button', {
                    title: "Insert Shortcode",
					image: yo_shortcodes.assets +"img/shortcodes.png",
					icons: false,
					onclick : function() { }
                });

                btn.onRenderMenu.add(function (c, b)
				{
					a.addWithPopup(b, "Accordion", "accordion");
					a.addWithPopup(b, "Button", "button");
					a.addWithPopup(b, "Columns", "columns");
					a.addImmediate(b, "Contact Form", "[yo_contact]");
					a.addWithPopup(b, "Hero Text", "hero");
					a.addImmediate(b, "Horizontal Rule", "[yo_hr]");
					a.addWithPopup(b, "Lists", "list");
					a.addWithPopup(b, "Maps", "maps");
					a.addWithPopup(b, "Messages", "message");
					a.addWithPopup(b, "Quote", "quote");
					a.addWithPopup(b, "Slider", "slider");
					a.addWithPopup(b, "Tabs", "tabs");
					a.addWithPopup(b, "Video", "video");
				});
                
                return btn;
			}
			
			return null;
		},
		addWithPopup: function (ed, title, id) {
			ed.add({
				title: title,
				onclick: function() {
					window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: title,
						identifier: id
					});
				}
			});
		},
		addImmediate:function(ed, title, shortcode){
			ed.add({
				title: title,
				onclick: function() {
					window.tinyMCE.activeEditor.execCommand("mceInsertContent", false, shortcode);
				}
			});
		},
		tb_resize: function()
    	{
 			var	iframeCont = jQuery('#TB_iframeContent'),
				tbWindow = jQuery('#TB_window'),
				popup = jQuery('#TB_iframeContent').contents().find('#yo-shortcodes');

			tbWindow.css({
                height: popup.outerHeight(),
                width: popup.outerWidth(),
                marginLeft: -(popup.outerWidth()/2),
            });

			iframeCont.css({
				paddingTop: 0,
				paddingLeft: 0,
				paddingRight: 0,
				height: (tbWindow.outerHeight()),
				overflow: 'auto', // !!!
				width: popup.outerWidth()
			});
    	},
        getInfo: function() {
            return {
                longname: "Yow Shortcodes",
                author: 'Giordano Piazza',
                authorurl: 'http://giordanopiazza.com',
                version: "1.0"
            };
        }
    });
    tinymce.PluginManager.add('yo_sc_button', tinymce.plugins.yoShortcodes);
})();


