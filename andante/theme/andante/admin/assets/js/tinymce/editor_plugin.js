/*
 * Shortcode Editor
 * v1.0
*/

(function() {

	/**
	 * Get String between a Prefix String and a Suffix String
	 * Usage  var you = 'hello you guys'.between('hello ',' guys');
	 * you = 'you';
	 */
	String.prototype.between = function(prefix, suffix) {
	  s = this;
	  var i = s.indexOf(prefix);
	  if (i >= 0) {
	    s = s.substring(i + prefix.length);
	  }
	  else {
	    return '';
	  }
	  if (suffix) {
	    i = s.indexOf(suffix);
	    if (i >= 0) {
	      s = s.substring(0, i);
	    }
	    else {
	      return '';
	    }
	  }
	  return s;
	}

    tinymce.PluginManager.add( 'yo_sc_button', function( editor, url ) {

    	var yo = this;

    	editor.addCommand('yoEditor', function(a, params) {
			// editor.windowManager.alert('Hello world!! Selection: ' + editor.selection.getContent({format : 'text'}));
			var shortcode = params.identifier;
				// active = params.active;

			// Load thickbox
			tb_show("Insert Shortcode", yo_shortcodes.editor_page + "&post_id=&shortcode=" + shortcode + /*"&width=" + 650+ "&height=" + 550 +*/ "&TB_iframe=1");

			// Wait for the iframe to load
			jQuery('#TB_iframeContent').load(function()
			{
				// Resize the popup
				// yo.tb_resize();
				// jQuery(window).resize(function() { yo.tb_resize(); });

				jQuery("#TB_iframeContent").contents().find('#yo-insert-shortcode').click(function()
				{
					var shortcode_template = jQuery("#TB_iframeContent").contents().find('#yo-shortcode-template').text();
		    		
		    		// Clone groups
		    		// Fill in the gaps eg {{param}}
		    		// TODO: Change 'input, textarea...' with the serialized form data
		    		jQuery("#TB_iframeContent").contents().find('[data-clone]').each(function()
		    		{
		    			// Get the clone group id and the template to use for the clones
		    			var clone_group = jQuery(this).attr('data-clone'),
		    				clone_template = shortcode_template.between('{{clone:'+clone_group+'}}','{{/clone}}'),
		    				form_fields = 'input, textarea, select, radio, checkbox',
		    				output = '';

		    			if (clone_template)
		    			{
		    				// Get the clones
		    				jQuery("#TB_iframeContent").contents().find('.'+clone_group+' .yo-clone').each(function()
		    				{
		    					var clone = clone_template;

			    				// Get the fields of each clone
			    				jQuery(form_fields, jQuery(this)).each(function(){
			    					var input = jQuery(this),
			    						id = input.attr('id').replace(clone_group+'_', ''),
					    				re = new RegExp("{{"+id+"}}","g");
					    			clone = clone.replace(re, input.val());				    					
			    				});
			    				output += clone;
		    				});
		    			}

		    			// Remove the loop tags from the shortcode
		    			shortcode_template = shortcode_template.replace('{{clone:'+clone_group+'}}'+clone_template+'{{/clone}}', output);
		    		});// clone group
					
		    		// Single fields
		    		// Fill in the gaps eg {{param}}
		    		// TODO: Change 'input, textarea...' with the serialized form data
					jQuery("#TB_iframeContent").contents().find('.yo-input input, .yo-input textarea, .yo-input select').each(function() {
		    			var input = jQuery(this),
		    				id = input.attr('id'),
		    				re = new RegExp("{{"+id+"}}","g");
		    			shortcode_template = shortcode_template.replace(re, input.val());
		    		});

					// Remove any {{/clone}}
		    		// shortcode_template = shortcode_template.replace('{{clone:'+clone_group+'}}'+clone_template+'{{/clone}}', output);

					if(window.tinyMCE)
					{
						window.tinyMCE.activeEditor.execCommand("mceInsertContent", false, shortcode_template);
                		tb_remove();
                	}
				});
			});
      	});

    	editor.addButton('yo_sc_button_key', {
            type: 'listbox',
            text: '[ / ] ',
            icon: false,
            tooltip: 'Shortcodes Editor',
            classes: 'widget btn yo-sc-button',
            onselect: function(e) {
            }, 
            values: [

                {text: 'Accordion', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Accordion',
						identifier: 'accordion'
					});
                }},

                {text: 'Button', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Button',
						identifier: 'button'
					});
                }},

                {text: 'Columns', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Columns',
						identifier: 'columns'
					});
                }},

                {text: 'Contact Form', onclick : function() {
                    tinymce.execCommand('mceInsertContent', false, '[yo_contact]');
                }},

                {text: 'Hero Text', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Hero Text',
						identifier: 'hero'
					});
                }},

                {text: 'Horizontal Rule', onclick : function() {
                    tinymce.execCommand('mceInsertContent', false, '[yo_hr]');
                }},

                {text: 'Lists', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Lists',
						identifier: 'list'
					});
                }},

				{text: 'Maps', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Maps',
						identifier: 'maps'
					});
                }}, 

                {text: 'Messages', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Messages',
						identifier: 'messages'
					});
                }}, 

                {text: 'Quote', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Quote',
						identifier: 'quote'
					});
                }}, 

                {text: 'Slider', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Slider',
						identifier: 'slider'
					});
                }}, 

                {text: 'Tabs', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Tabs',
						identifier: 'tabs'
					});
                }}, 

                {text: 'Video', onclick : function() {
                    window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: 'Video',
						identifier: 'video'
					});
                }}
            ]
        });

    } );

})();