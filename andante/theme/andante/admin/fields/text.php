<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Text_Field' ) )
{
	class YO_Text_Field
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $meta, $field)
		{
			// echo 'name: ' . $field['field_name'].'<br>';

			$name     = " name='{$field['field_name']}'";
			$id       = isset($field['clone']) && $field['clone'] ? '' : " id='{$field['id']}'";
			$val      = " value='{$meta}'";
			$size     = isset($field['size']) ? $field['size'] : '30';
			$std      = isset($field['disabled']) ? $field['disabled'] : false;
			
			$disabled = disabled($std, true, false);

			$html    = "<input type='text' {$name}{$id}{$val}{$disabled} size='{$size}' />";

			return $html;
		}
	}
}