<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Html_Field' ) )
{
	class YO_Html_Field
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $meta, $field)
		{
			// TODO: Add some placeholders to be automatically substituted, like {id} etc...
		
			$html = $field['html'];

			return $html;
		}
	}
}