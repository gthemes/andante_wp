<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Media_Field' ) )
{

	add_action('wp_ajax_get_media', array('YO_Media_Field', 'get_media'), 0);


	class YO_Media_Field
	{
		static function add_actions()
		{
			// add_action('wp_ajax_plupload', array('Turbine_Plupload_Field', 'upload'), 0);
			// add_action('wp_ajax_get_media', array(__CLASS__, 'get_media'), 0);
			// add_filter( "yo_plupload_end_html", array(__CLASS__, '_end_html'), 10, 3);
		}

		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{

			// Javascript
			wp_enqueue_media();
			wp_enqueue_script('yo-medialibrary', YO_JS_URL.'media.js', array('jquery'), YO_VER);
			wp_localize_script('yo-medialibrary', 'yo_media', array('_ajax_nonce' => wp_create_nonce('medialibrary')));
			// wp_enqueue_script('yo-ajaxupload', YO_JS_URL.'ajaxupload.js', array('jquery'), YO_VER);
			// wp_enqueue_script('yo-medialibrary', YO_JS_URL.'medialibrary-uploader.js', array('jquery', 'yo-ajaxupload'), YO_VER);
			// add_action('wp_ajax_get_media', array(__CLASS__, 'get_media'), 0);
		}


		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $meta, $field)
		{
			$name     = " name='{$field['field_name']}'";
			$id       = isset($field['clone']) && $field['clone'] ? '' : " id='{$field['id']}'";
			$val      = " value='{$meta}'";
			$std      = isset($field['disabled']) ? $field['disabled'] : false;
			$disabled = disabled( $std, true, false );
			$label 	  = (isset($field['label'])) ? " value='{$field['label']}'" : '';
			$desc 	  = $field['desc'];
			$mod 	  = (isset($field['mod'])) ? $field['mod'] : 'min';
			$thumb 	  = (isset($field['thumb'])) ? $field['thumb'] : 'thumbnail';

			$html = '';
			$hide = '';
			
			if ($mod == "min") { $hide ='hide'; }
				    
			$html .= '<div class="yo-media-thumb">';

			if(!empty($meta))
			{
				$thumb = wp_get_attachment_image_src($meta, $thumb);

				$html .= '<a class="yo-uploaded-image image_'.$field['id'].'" href="'. $meta . '">';
		    	$html .= '<img src="'.$thumb[0].'" alt="" />';
		    	// $html .= '<img src="'.$meta.'" alt="" />';
		    	$html .= '</a>';			
			}

			$html .= '</div>';


			// This field stores the media attachment ID
			$html  .= "<input type='text' class='{$hide}'{$name}{$id}{$val} />";

			// This stores the media attachment URL
			// $name_url = " name='{$field['field_name']}";
			// $html .= "<input type='hidden'{$name_url}{$val} />";
			
			// The upload button
			$html .= '<div class="yo-media-buttons"><span class="button yo-upload-media '.$field['id'].'_upload">'.__('Insert File', 'theme_admin').'</span>';
			
			$hide = (empty($meta)) ? 'hide' : '';

			$html .= '<span class="button yo-remove-media reset_'. $field['id'] .' '. $hide.'" data-remove="' . $field['id'] . '">'.__('Remove', 'theme_admin').'</span>';
			$html .='</div>' . "\n";


			return $html;
		}


		static function get_thumbnail($id, $size = 'thumbnail', $force_icon = false)
		{
			// $mimetype = self::get_mimetype($id);

			// if ($mimetype['image'] === true && $force_icon === false)
			// {
				$thumb = wp_get_attachment_thumb_url($id, $size);
			// }
			// else
			// {
				// $thumb = $mimetype['icon'];
			// }
			
			$file = wp_get_attachment_url($id);

			$output  = '<a class="yo-uploaded-image" href="'. $file . '" target="_blank">';
			$output .= '<img src="'.$thumb.'" alt="'.basename($file).'" />';
	    	$output .= '</a>';

	    	return $output;
		}


		static function get_media()
		{
			// global $post;

		    check_ajax_referer('medialibrary');

		    $id = (int) $_POST['id'];

		    echo self::get_thumbnail($id);

			// $file = wp_get_attachment_metadata($id);

			// if (empty($file))
			// {
			// 	// not an image
			// }
			// else
			// {

			// }

		    // header('Content-Type: application/json');
		    // echo json_encode($file);


		    exit;
		}

	// 	/**
	// 	 * Native media library uploader
	// 	 *
	// 	 * @uses get_option()
	// 	 *
	// 	 * @access public
	// 	 * @since 1.0.0
	// 	 *
	// 	 * @return string
	// 	 */
	// 	public static function media_uploader($meta, $id, $name, $std, $int, $mod)
	// 	{
	// 	    // $options_data = get_option(THEME_OPTIONS);
			
	// 		$uploader = '';
	// 	    // $upload = (isset($options_data[$id])) ? $options_data[$id] : '';
	// 		$hide = '';
			
	// 		// print_r($upload);

	// 		if ($mod == "min") {$hide ='hide';}
			
	// 	    if ($meta != '') { $val = $meta; } else { $val = $std; }
		    
	// 		// $html    .= "<input type='text' class='yo-text'{$name}{$id}{$val}{$disabled} size='{$size}' />";

	// 		$uploader .= '<input type="text" class="'.$hide.' upload of-input" name="'. $id .'" id="'. $id .'_upload" value="'. $val .'" />';	
			
	// 		$uploader .= '<div class="upload_button_div"><span class="button media_upload_button" id="'.$id.'" rel="' . $int . '">Upload</span>';
			
	// 		if(!empty($meta)) {$hide = '';} else { $hide = 'hide';}
	// 		$uploader .= '<span class="button mlu_remove_button '. $hide.'" id="reset_'. $id .'" title="' . $id . '">Remove</span>';
	// 		$uploader .='</div>' . "\n";
	// 		$uploader .= '<div class="screenshot">';
	// 		if(!empty($meta)){	
	// 	    	$uploader .= '<a class="of-uploaded-image" href="'. $meta . '">';
	// 	    	$uploader .= '<img class="of-option-image" id="image_'.$id.'" src="'.$meta.'" alt="" />';
	// 	    	$uploader .= '</a>';			
	// 			}
	// 		$uploader .= '</div>';
	// 		$uploader .= '<div class="clear"></div>' . "\n"; 
		
	// 		return $uploader;
			
	// 	}
	}
}