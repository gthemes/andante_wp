<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Date_Field' ) )
{
	class YO_Date_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			// CSS
			wp_register_style( 'jquery-ui-core', YO_CSS_URL."jqueryui/jquery.ui.core.css", array(), '1.8.17' );
			wp_register_style( 'jquery-ui-theme', YO_CSS_URL."jqueryui/jquery.ui.theme.css", array(), '1.8.17' );
			wp_enqueue_style( 'jquery-ui-datepicker', YO_CSS_URL."jqueryui/jquery.ui.datepicker.css", array( 'jquery-ui-core', 'jquery-ui-theme' ), '1.8.17' );

			// Javascript
			wp_register_script( 'jquery-ui-datepicker', YO_JS_URL."jqueryui/jquery.ui.datepicker.min.js", array( 'jquery-ui-core' ), '1.8.17', true );
			wp_enqueue_script( 'yo-date', YO_JS_URL. 'date.js', array( 'jquery-ui-datepicker' ), YO_VER, true );
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{
			$name   = " name='{$field['field_name']}'";
			$id     = isset( $field['clone'] ) && $field['clone'] ? '' : " id='{$field['id']}'";
			$format = " rel='{$field['format']}'";
			$val    = " value='{$meta}'";
			$html   = "<input type='text' {$name}{$id}{$format}{$val} size='30' />";

			return $html;
		}
	}
}