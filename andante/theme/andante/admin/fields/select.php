<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Select_Field' ) )
{
	class YO_Select_Field
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $meta, $field)
		{
			if ( ! is_array($meta))
				$meta = (array) $meta;

			$std		 = isset($field['disabled']) ? $field['disabled'] : false;
			$disabled	 = disabled($std, true, false);

			$id		 = " id='{$field['id']}'";
			$name	 = " name='{$field['field_name']}'";
			$name	.= $field['multiple'] ? " multiple='multiple'" : "" ;

			$html	 = "<select {$name}{$id}{$disabled}>";
			foreach ($field['options'] as $key => $value)
			{
				$selected	 = selected( in_array($key, $meta), true, false);
				$html		.= "<option value='{$key}'{$selected}>{$value}</option>";
			}
			$html	.= "</select>";

			return $html;
		}
	}
}