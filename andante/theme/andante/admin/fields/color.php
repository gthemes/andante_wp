<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Color_Field' ) )
{
	class YO_Color_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_style( 'yo-color', YO_CSS_URL.'color.css', array( 'farbtastic' ), YO_VER );
			wp_enqueue_script( 'yo-color', YO_JS_URL.'color.js', array( 'farbtastic' ), YO_VER, true );
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{
			if ( empty( $meta ) )
				$meta = '#';
			$name  = " name='{$field['field_name']}'";
			$id    = isset( $field['clone'] ) && $field['clone'] ? '' : " id='{$field['id']}'";
			$value = " value='{$meta}'";

			$html = <<<HTML
<input class="yo-color" type="text"{$name}{$id}{$value} size="8" />
<div class="yo-color-picker"></div>
HTML;

			return $html;
		}
	}
}