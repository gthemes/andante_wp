<?php get_header(); ?>

      <!-- Content -->
      <section id="content" role="main" class="container">

      		<!-- 404 Error Message -->
            <section class="span8 one-column">
                  <h1><?php _e("Ooops! The page you're looking for doesn't exist!", 'theme_admin'); ?></h1>
            </section> 


            <!-- Sidebar -->
            <aside class="span4">
                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar')) : ?>
                <?php endif; ?>
            </aside> <!-- / Sidebar -->

      </section> <!-- / #content -->

<?php get_footer(); ?>