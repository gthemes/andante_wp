<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  	<meta charset="<?php bloginfo( 'charset' ); ?>">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  	<!-- Title & Meta Tags -->
	<title><?php wp_title(' | ', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Responsive Images -->
	<script>document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';</script>
<?php wp_head(); ?>
</head>
<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<body <?php body_class(); ?>>

	<!-- Logo, Headline, Navigation -->
	<header id="main-header" class="container">
	  	<div class="span12">

	  		<div id="logo">
	  			<a href="<?php echo site_url(); ?>">
		  			<?php if (get_field('logo_enabled', 'options')) : ?>
			  			<img src="<?php the_image(get_field('logo', 'options')); ?>" alt="<?php bloginfo('name'); ?>">
			  		<?php else : ?>
			  			<h1><?php bloginfo('name'); ?></h1>
			  		<?php endif; ?>
		  		</a>
		  		<span id="tagline"><?php bloginfo('description'); ?></span>
	  		</div>

	  		<div id="nav-toggle">
		      	<a href="#" data-target=".expandable-nav" class="button"><?php _e('Menu', 'theme_admin'); ?></a>
		    </div>

	  		<nav id="main-nav" class="clearfix">
				<?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'dropdown', 'fallback_cb' => false, 'walker' => new YO_Walker() )); ?>
	  		</nav>
	  	</div>
	</header>