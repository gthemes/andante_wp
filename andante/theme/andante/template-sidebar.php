<?php
/*
Template Name: Page with Sidebar
*/

get_header(); ?>

      <!-- Content -->
      <section id="content" role="main" class="container">

            <h1 class="span12"><?php the_title(); ?></h1>
    
            <div class="span12 hr_pattern"></div>

            <!-- Posts -->
            <section class="span8 one-column">
                  <!-- Start the Loop. -->
                  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                  
                  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                  <!-- Display the Post's Content in a div box. -->
                  <div class="entry">
                  <?php the_content(); ?>
                  </div>

                  </div> <!-- closes the first div box -->

                  <!-- Stop The Loop -->
                  <?php endwhile; ?>

                  <!-- Next/Previous Pge -->
                  <?php
                      echo previous_posts_link(__('&larr; Previous page &nbsp;&nbsp;&nbsp;', 'theme_admin'));
                      echo next_posts_link(__('Next page &rarr;', 'theme_admin'));
                  ?>

                  <?php else: ?>

                  <p><?php _e('Sorry, no pages matched your criteria.', 'theme_admin'); ?></p>

                  <!-- REALLY stop The Loop. -->
                  <?php endif; ?>
            </section> <!-- / Posts -->


            <!-- Sidebar -->
            <aside class="span4">
                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar')) : ?>
                <?php endif; ?>
            </aside> <!-- / Sidebar -->

      </section> <!-- / #content -->

<?php get_footer(); ?>