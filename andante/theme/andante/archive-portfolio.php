<?php get_header(); ?>

      <?php
        /* PREVIEW */
        if (isset($_GET['columns']))
        {
          $valid_columns = array(2, 3, 4);
          if (in_array($_GET['columns'], $valid_columns))
            $columns = $_GET['columns'];

          if ( ! isset($columns)) $columns = 3;
          $ppp = ($columns == 4 || $columns == 2) ? 8 : -1;
        }
        else
        {
          $columns = get_field('portfolio_columns', 'option');
          if (!$columns) $columns = 3;
          $ppp = -1;
        }
        /* / PREVIEW */

        // $ppp = 3;
        $args = '&post_type=portfolio&post_status=publish&order=ASC&orderby=menu_order&posts_per_page='.$ppp;

        global $wp_query;
        if (is_singular()) $page_title = get_the_title($wp_query->get_queried_object_id());
        if (!isset($page_title) || empty($page_title)) $page_title = __('Portfolio', 'theme_admin');
        
        global $query_string;
        query_posts($query_string.$args);

        $is_sub_term = (isset(get_queried_object()->term_id)) ? true : false;

        if ($is_sub_term)
        {
          $page_title = get_queried_object()->name;
          $terms = get_term_children(get_queried_object()->term_id, 'portfolio-categories');
        }
        else
        {
          

          // $page_title = get_queried_object()->name;

          // $page_title = get_query_var('pagename');
          // echo '<pre>';
          // print_r(get_queried_object());
          // echo '</pre>';

          $terms = get_terms('portfolio-categories', array('parent' => 0));
        }

        $terms_count = count($terms);

      ?>

      <!-- Content -->
      <section id="content" role="main" class="container">

            <!-- Page heading and Navigation -->
            <h1 class="span12"><?php echo $page_title; ?></h1>
            <div class="span12 hr_pattern"></div>

            <?php $span = ($columns) ? 'span'.floor(12 / $columns) : 'span3'; ?>

            <?php if ($terms_count > 0) : ?>

              <ul class="span12 filter">

              <?php 
                  // $categories = get_terms('portfolio-categories', array('parent' => 0));

                  echo '<li class="active"><a href="#all">'.__('All', 'theme_admin').'</a> <span class="divider">/</span></li>';

                  $i = 0;
                  foreach($terms as $term)
                  {
                      if ($is_sub_term) $term = get_term_by('id', $term, 'portfolio-categories');
                      echo '<li><a href="#'.$term->slug.'">'.$term->name.'</a>';
                      if (++$i < $terms_count)
                          echo '<span class="divider">/</span>';
                      echo '</li>';
                  }
                  unset($terms);
              ?>

              </ul>

            <?php endif; ?>

            <!-- Portfolio items wrapper -->
            <div class="filterable">
   
            <!-- Start the Loop. -->
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php
              $class = '';
              $terms = get_the_terms(get_the_ID(), 'portfolio-categories');
              if ($terms) 
              {
                foreach ($terms as $term)
                $class .= strtolower(' '.$term->slug);
              }

            ?>

            <!-- Portfolio item -->
            <div class="<?php echo $span.$class; ?>">
                  <div class="media">
                        <?php if (get_field('yo_portfolio_enlarge') == true) : ?>
                          <a class="object" data-hover="icon-zoom" href="<?php the_image(get_post_thumbnail_id()); ?>" data-rel="gallery[portfolio]"><?php the_post_thumbnail('portfolio-'.$columns.'-columns', array('title' => get_the_title())); ?></a>
                        <?php else: ?>
                          <a class="object" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('portfolio-'.$columns.'-columns', array('title' => get_the_title())); ?></a>
                        <?php endif; ?>
                        <div class="caption">
                              <h3><?php the_title(); ?></h3>
                              <div class="hr_small"></div>
                              <p><?php the_excerpt(); ?></p>
                              <p><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="button mini"><?php _e('More &rarr;', 'theme_admin') ?></a></p>
                        </div>
                  </div>
            </div> <!-- / Portfolio item -->

            <!-- Stop The Loop. -->
            <?php endwhile; else: ?>

            <p><?php _e('Sorry, no pages matched your criteria.', 'theme_admin'); ?></p>

            <!-- REALLY stop The Loop. -->
            <?php endif; ?>

            </div> <!-- / .filterable -->

      </section> <!-- / #content -->

<?php 

global $wp_query;
$big = 999999999;

$pages = paginate_links( array(
    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
    'format' => '?paged=%#%',
    'current' => max(1, get_query_var('paged')),
    'total' => $wp_query->max_num_pages
));

echo $pages;

?>

<?php get_footer(); ?>