<?php
/*
Template Name: No Title
*/

get_header(); ?>

	<!-- No title Page -->
	<section id="content" role="main" class="container">
		
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    	<div class="span12 hr_pattern"></div>
    	
        <?php the_content(); ?>

        <?php endwhile; endif; ?>

	</section>

<?php get_footer(); ?>