<?php get_header(); ?>


      <!-- Portfolio Details Page -->
      <section id="content" role="main" class="container">


            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h1 class="span8"><?php the_title(); ?></h1>


            <!-- Arrows navigation -->
            <div class="span4">
                  <ul class="projects-nav">
                        <!-- Previous item -->
                        <?php if (get_previous_post()) : ?>
                        <li class="nav-prev"><?php previous_post_link('%link', '<i class="icon-arrow-left"></i>'); ?></li>
                        <?php endif; ?>
                        
                        <!-- Back to the overview -->
                        <li><a href="<?php echo get_post_type_archive_link('portfolio'); ?>" title="<?php _e('Back to the overview', 'theme_admin'); ?>"><i class="icon-overview"></i></a></li>

                        <!-- Next item -->
                        <?php if (get_next_post()) : ?>
                        <li class="nav-next"><?php next_post_link('%link', '<i class="icon-arrow-right"></i>'); ?></li>
                        <?php endif; ?>
                  </ul>
            </div>


            <!-- Images slider -->
            <div class="span8">
                  <div id="details">
                  <?php
                        $content_type = get_field('yo_content_type');

                        if ( ! empty($content_type))
                              get_template_part('library/cformat-'.$content_type);

                        // if ( ! empty($content_type))
                              // get_template_part('portfolio', $content_type);
                  ?>
                  </div>

                  <div id="portfolio-content">
                        <div class="nested">
                              <?php the_content(); ?>
                        </div>
                  </div>

            </div>


            <!-- Project details -->
            <div class="span4">
                  <?php $header_1 = get_field('yo_header_1');
                  if ( ! empty($header_1)) : ?>
                        <h3><?php echo $header_1; ?></h3>
                  <?php endif; ?>

                  <!-- Additional project links -->
                  <?php $project_links = get_field('yo_project_links');
                  if ( ! empty($project_links)) : 
                        foreach ($project_links as $link) : ?>
                              <p class="meta"><?php echo $link['yo_link_text']; ?> &nbsp;&nbsp;<a href="<?php echo $link['yo_link_url']; ?> " target="_blank" class="micro button">&rarr;</a></p>
                  <?php endforeach; endif; ?>

                  <!-- Project date -->
                  <?php $project_date = get_field('yo_project_date');
                  if ( ! empty($project_date)): ?>
                        <p class="meta"><?php _e('Project Date:', 'theme_admin'); ?> <?php the_field('yo_project_date'); ?></p>
                  <?php endif; ?>
                  
                  <!-- First paragraph -->
                  <?php $paragraph_1 = get_field('yo_paragraph_1');
                  if ( ! empty($paragraph_1)): ?>
                        <?php the_field('yo_paragraph_1'); ?>
                  <?php endif; ?>

                  <!-- Second paragraph -->
                  <?php $block_toggle = get_field('yo_block_toggle');
                        $test = get_field('yo_test_box');
                  if ($block_toggle):
                        $header_2 = get_field('yo_header_2');
                        if ( ! empty($header_2)) : ?>
                              <h3><?php echo $header_2; ?></h3>
                        <?php endif; ?>

                        <?php $paragraph_2 = get_field('yo_paragraph_2');
                        if ( ! empty($paragraph_2)): ?>
                              <?php echo do_shortcode($paragraph_2); ?>
                        <?php endif; ?>

                  <?php endif; ?>

                  <!-- Main project link -->
                  <?php $project_link = get_field('yo_project_link'); /*echo '<pre>'; print_r($project_link); echo '</pre>';*/
                  // print_r($project_link);
                  /*$project_link = unserialize($project_link[0]); $project_link = $project_link[0];*/ ?>
                  <?php if (isset($project_link['url']) && ! empty($project_link['url'])) : ?>
                        <p><a href="<?php echo $project_link['url']; ?>" class="button full" target="_blank"><?php if (isset($project_link['label'])) echo $project_link['label']; ?></a></p>
                  <?php endif; ?>
            </div>

            <!-- Stop The Loop (but note the "else:" - see next line). -->
            <?php endwhile; else: ?>

            <p><?php _e('Sorry, no pages matched your criteria.', 'theme_admin'); ?></p>

            <!-- REALLY stop The Loop. -->
            <?php endif; ?>

      </section>

<?php get_footer(); ?>