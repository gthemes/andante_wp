<?php
/*
Template Name: Home Page
*/

get_header(); 

?>

      <!-- Content Slider -->
      <div id="slider" class="container">
            <div class="content-slider span12">
                  <div class="flexslider">
                        <ul class="slides">
                              <?php 
                              $slides = get_field('home_slider', 'option');

                              if ( ! empty($slides)): ?>
                              <?php foreach ($slides as $slide) : /*echo '<pre>'; print_r($slide); echo '</pre>';*/ $image_resized = wp_get_attachment_image_src($slide['media_item'], 'full'); ?>
                                  <li>
                                        <?php if (!empty($slide['url'])) : ?><a href="<?php echo $slide['url']; ?>"><?php endif; ?>
                                        <img src="<?php the_image($slide['media_item']); ?>" alt="<?php echo $slide['title']; ?>" />
                                        <?php if ( ! empty($slide['caption']) && $slide['caption_type'] != 'none') : ?>
                                        <p class="<?php echo $slide['caption_type']; ?>"><?php echo $slide['caption']; ?></p>
                                        <?php if (!empty($slide['url'])) : ?></a><?php endif; endif; ?>

                                  </li>
                              <?php endforeach; endif; ?>
                        </ul>
                  </div>
            </div>
      </div> <!-- / #slider -->


      <?php if (function_exists('dynamic_sidebar') && is_active_sidebar('below-slideshow-widgets')) : ?>
            <div class="container">
                  <div class="span12 hr_pattern" style="margin-top:10px"></div>
                  <?php dynamic_sidebar('below-slideshow-widgets'); ?>
            </div>
      <?php endif; ?>


      <!-- Features -->
      <?php
            if (get_field('features_enabled', 'option')):
                  $features = get_field('features', 'option');
                  $features_count = count($features);

                  if ($features_count > 4) $features_count = 4;
                  if ($features_count < 1) $features_count = false;

                  switch ($features_count)
                  {
                        case 1:
                              $span = 'span12';
                              break;
                        case 2:
                              $span = 'span6';
                              break;
                        case 3:
                              $span = 'span4';
                              break;
                        case 4:
                              $span = 'span3';
                              break;
                        default:
                              $span = '';
                              break;
                  }

                  if ($features_count > 0) { ?>

                        <div id="features" class="container">
                              <div class="span12 hr_pattern"></div>

                        <?php foreach ($features as $feature) : ?>
                              <div class="<?php echo $span; ?>">
                                    <?php if (isset($feature['media_item'])) : ?>
                                          <?php if (!empty($feature['link'])) : ?><a href="<?php echo $feature['link']; ?>" target="<?php echo (!empty($feature['link_target'])) ? $feature['link_target'] : '_self'; ?>"><?php endif; ?>
                                                <img src="<?php the_image($feature['media_item'], 'thumbnail'); ?>" alt="<?php echo $feature['title']; ?>">
                                          <?php if (!empty($feature['link'])) : ?></a><?php endif; ?>
                                    <?php endif; ?>
                                    <?php if (!empty($feature['title'])) : ?><h2><?php echo $feature['title']; ?></h2><?php endif; ?>
                                    <?php if (!empty($feature['text'])) : ?><p><?php echo $feature['text']; ?></p><?php endif; ?>
                              </div>
                        <?php endforeach; ?> 

                        </div> <!-- / #features -->
                  <?php } endif; ?>

      <!-- Content -->
      <section id="content" role="main" class="container">
            
            <?php
                  $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'portfolio', 'post_status' => 'publish', 'meta_query' => array('relation' => 'AND', array('key' => 'is_sticky', 'value' => '1', 'compare' => '=', 'type' => 'CHAR')));
                  // $args = array('orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 3, 'post_type' => 'portfolio', 'post_status' => 'publish');
                  $sticky_posts = new WP_Query($args);
                  $featured = get_field('featured_projects', 'option');

                  if (!empty($featured))
                  {
                        $span = ($featured['columns']) ? 'span'.floor(12 / $featured['columns']) : 'span4';
                        switch ($featured['columns'])
                        {
                              case 2:
                                    $columns_variation = 'two-columns';
                                    break;
                              case 3:
                              default:
                                    $columns_variation = 'three-columns';
                                    break;
                              case 4:
                                    $columns_variation = 'four-columns';
                                    break;
                        }
                  }
            ?>
            
            <?php if ( isset($featured['enabled']) && !empty($featured['enabled'])) : ?>
            
                  <!-- 2 Columns -->
                  <div class="<?php echo $columns_variation; ?>">

                        <!-- Heading Text -->
                        <?php if ( ! empty($featured['title'])) : ?>
                              <div class="striped-heading span12">
                                    <h1><?php echo $featured['title']; ?></h1>
                                    <div class="decoration"></div>
                              </div>
                        <?php endif; ?>

                        <?php if ($sticky_posts->have_posts()) : while ($sticky_posts->have_posts()) : $sticky_posts->the_post(); ?>

                              <!-- Post -->
                              <?php
                                    $content_type = get_field('yo_content_type', get_the_ID());
                                    @include(locate_template('library/cformat-image.php'));

                                    // if ( ! $content_type) {
                                    //       // get_template_part('library/cformat-standard');
                                    //       include(locate_template('library/cformat-image.php'));
                                    // } else {
                                    //       // get_template_part('library/cformat-'.$content_type);
                                    //       @include(locate_template('library/cformat-'.$content_type.'.php'));
                                    // }
                              ?> <!-- / Post -->
                  
                        <?php endwhile; endif; ?>

                  </div>

            <?php endif; ?>

      </section> <!-- / #content -->

<?php get_footer(); ?>