<?php

// Reporting E_NOTICE can be good too (to report uninitialized
// variables or catch variable name misspellings ...)
// error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);



/*
 *------------------------------------------------------------------------------------------------
 * Theme Setup
 *------------------------------------------------------------------------------------------------
 *
 * Do some stuff after installing the theme.
 *
 */

function yo_theme_setup()
{
    //----------------------------------------------------------------------------------------
    // Languages
    //----------------------------------------------------------------------------------------
    load_theme_textdomain('theme_admin', get_template_directory() . '/languages');
    $locale = get_locale();
    $locale_file = get_template_directory() . "/languages/$locale.php";
    if (is_readable($locale_file))
        require_once($locale_file);

    
    //----------------------------------------------------------------------------------------
    // Menus
    //----------------------------------------------------------------------------------------
    register_nav_menus(array(
        'primary' => __('Main Menu', 'theme_admin'),
        // 'side' => __('Side Menu', 'theme_admin')
    ));


    //----------------------------------------------------------------------------------------
    // Post Formats (WP3.1+)
    //----------------------------------------------------------------------------------------
    $formats = array(/*'aside',*/ 'audio', 'gallery', 'image', 'link', 'quote', 'video', 'status');
    add_theme_support('post-formats', $formats); 


    //----------------------------------------------------------------------------------------
    // Thumbnails (Custom sizes)
    //----------------------------------------------------------------------------------------
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 83, 55, true ); // Normal post thumbnails
    // add_image_size('single-post', 400, false, true); // Blog post
    add_image_size('full-width', 1124, false, true); // Home slider images
    add_image_size('portfolio-details', 744, false, true); // Portfolio details page
    add_image_size('portfolio-4-columns', 269, false, true); // Portfolio overview with 4 columns
    add_image_size('portfolio-3-columns', 364, false, true); // ... 3 columns & Single post page
    add_image_size('portfolio-2-columns',  554, false, true); // ... 2 columns  


    //----------------------------------------------------------------------------------------
    // Custom Headers (WP3.4+)
    //----------------------------------------------------------------------------------------
    // $headers = array(
        // The height and width of our custom header.
        // 'width' => apply_filters( 'yo_header_image_width', 1124 ),
        // 'height' => apply_filters( 'yo_header_image_height', 550 ),
        // 'flex-height'   => true,
        // 'default-image' => get_template_directory_uri() . '/img/headers/header_1.jpg',
        // 'uploads'       => true,
        // Callback for styling the header.
        // 'wp-head-callback' => 'twentyeleven_header_style',
        // Callback for styling the header preview in the admin.
        // 'admin-head-callback' => 'twentyeleven_admin_header_style',
        // Callback used to display the header preview in the admin.
        // 'admin-preview-callback' => 'twentyeleven_admin_header_image',
    // );
    // add_theme_support('custom-header', $headers);

    // Default custom headers included with the theme.
    // register_default_headers(array(
    //     'Green' => array(
    //         'url' => '%s/img/headers/header_1.jpg',
    //         'thumbnail_url' => '%s/img/admin/dark.jpg',
    //         'description' => __('Green', 'theme_admin')
    //     ),
    //     'Forest' => array(
    //         'url' => '%s/img/headers/header_2.jpg',
    //         'thumbnail_url' => '%s/img/admin/forest.jpg',
    //         'description' => __('Forest', 'theme_admin')
    //     ),
    // ));


    //----------------------------------------------------------------------------------------
    // Custom Backgrounds (WP3.4+)
    //----------------------------------------------------------------------------------------
    $backgrounds = array(
        'default-color'          => '000',
        'default-image'          => get_template_directory_uri() . '/img/content/background.png',
        // 'wp-head-callback'       => '_custom_background_cb',
        // 'admin-head-callback'    => '',
        // 'admin-preview-callback' => ''
    );
    // add_theme_support('custom-background', $backgrounds);


    //----------------------------------------------------------------------------------------
    // Add default posts and comments RSS feed links to <head>.
    //----------------------------------------------------------------------------------------
    add_theme_support('automatic-feed-links');


    //----------------------------------------------------------------------------------------
    // TinyMCE Style
    //----------------------------------------------------------------------------------------
    // Customize the visual editor style with editor-style.css to match the theme style.
    // add_editor_style('style.css');
}

add_action('after_setup_theme', 'yo_theme_setup');



/*
 *------------------------------------------------------------------------------------------------
 * Admin bar Menu Items
 *------------------------------------------------------------------------------------------------
 *
 * Add some items to the admin menu, like the theme-options link to quickly customize the theme.
 *
 */

function admin_menu_items($admin_bar)
{
    $args = array(
            'id'    => 'theme-options',
            'title' => __('Theme Options', 'theme_admin'),
            'href'  => site_url() .'/wp-admin/themes.php?page=yo-theme-options',
            'meta'  => array('title' => __('Click to edit the theme options', 'theme_admin'))
            );
 
    $admin_bar->add_menu($args);
}

add_action('admin_bar_menu', 'admin_menu_items',  100);



/*
 *------------------------------------------------------------------------------------------------
 * Register Widgets (Sidebars)
 *------------------------------------------------------------------------------------------------
 *
 * These are the widget areas that can be customized in the Admin section (Appearance > Widgets).
 *
 */

function yo_widgets_init()
{
    // Portfolio Widget
    include_once('library/widget-portfolio.php');
    register_widget('Projects_Widget');

    // Better Recent Posts Widget
    include_once('library/widget-recent-posts.php');
    register_widget('YO_Better_Recent_Posts');

    // Latest tweets Widget
    include_once('library/widget-twitter.php');
    register_widget('YO_Twitter_Widget');

    // Flickr Widget
    include_once('library/widget-flickr.php');
    register_widget('YO_Hey_Its_Flickr');

    register_sidebar(array(
        'name' => 'Sidebar',
        'id' => 'siderbar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'));

    register_sidebar(array(
        'name' => 'Below Home Slideshow',
        'id' => 'below-slideshow-widgets',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3><div class="hr_small"></div>'));

    register_sidebar(array(
        'name' => 'Above Footer',
        'id' => 'above-footer-widgets',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3><div class="hr_small"></div>'));

    register_sidebar(array(
        'name' => 'Footer',
        'id' => 'footer-widgets',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3><div class="hr_small"></div>'));
}

add_action('widgets_init', 'yo_widgets_init');



/*
 *------------------------------------------------------------------------------------------------
 * Theme Notices & Plugins Install
 *------------------------------------------------------------------------------------------------
 *
 */

// TODO: check the installed plugins here
// if ( ! function_exists('some_plugin_function') && current_user_can('install_plugins'))
if (current_user_can('install_plugins'))
{
    /* Display a notice that can be dismissed */
    // add_action('admin_notices', 'yo_install_plugin_notice');

    function yo_install_plugin_notice()
    {
        $screen = get_current_screen();

        global $current_user;
        $user_id = $current_user->ID;
        /* Check that the user hasn't already clicked to ignore the message */
        if ( ! get_user_meta($user_id, 'yo_install_plugin_notice')) {
            add_thickbox();
            if ($screen->base != 'appearance_page_yo-shortcodes')
            {
                echo '<div class="updated"><p>';
                printf(__('If you wish to use custom post types for portfolios, please install the XXX Plugin.  <a href="%1$s" class="thickbox onclick">Install Now</a> | <a href="%2$s">Hide Notice</a>', 'theme_admin'), admin_url() . 'plugin-install.php?tab=plugin-information&plugin=plugin-name&TB_iframe=true&width=640&height=517', '?example_nag_ignore=0');
                echo '</p></div>';
            }
        }
    }

    // add_action('admin_init', 'yo_post_plugin_ignore');

    function yo_post_plugin_ignore()
    {
        global $current_user;
        $user_id = $current_user->ID;
        /* If user clicks to ignore the notice, add that to their user meta */
        if (isset( $_GET['example_nag_ignore']) && '0' == $_GET['example_nag_ignore']) {
            add_user_meta($user_id, 'example_ignore_notice', 'true', true);
        }
    }
}



/*
 *------------------------------------------------------------------------------------------------
 * Yow Framework
 *------------------------------------------------------------------------------------------------
 *
 * Initalize the Yow Framework.
 * In the admin it makes the meta box and theme options fields available, while in the front-end
 * it fetches the stored data to use in theme pages.
 *
 */

include('admin/init.php');



/*
 *------------------------------------------------------------------------------------------------
 * Load utilities and custom elements
 *------------------------------------------------------------------------------------------------
 *
 */

// Load useful helper functions
include('library/utilities.php');

// if (is_admin())
// {
    // Add custom post type 'Portfolio'. It includes its meta boxes setup.
    include('library/type-portfolio.php');

    // Load the theme options
    include('library/theme-options.php');
// }

// Load the 'Menu Walker' class
include('library/walker.php');

// Load the Shortcodes
include('library/shortcodes.php');





/*
 *------------------------------------------------------------------------------------------------
 * Enqueue CSS & Javascript in the frontend
 *------------------------------------------------------------------------------------------------
 *
 */

// Enqueue scripts
function yo_enqueue_scripts()
{
    // <!-- Stylesheets -->
    // <link href='http://fonts.googleapis.com/css?family=Armata' rel='stylesheet' type='text/css'>
    // <link rel="stylesheet" href="/normalize.css">
    // <link rel="stylesheet" href="grid.css">
    // <link rel="stylesheet" href="flexslider.css">
    // <link rel="stylesheet" href="prettyPhoto.css">
    // <link rel="stylesheet" href="">

    // <!-- Custom Modernizr -->
    // <script src="/js/libs/modernizr.custom.min.js"></script>

    if (is_singular() && comments_open() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');


    wp_enqueue_style('yo-armata', 'http://fonts.googleapis.com/css?family=Armata');
    wp_enqueue_style('yo-normalize', get_template_directory_uri().'/normalize.css', false, YO_VER);
    wp_enqueue_style('yo-grid', get_template_directory_uri().'/grid.css', false, YO_VER);
    wp_enqueue_style('yo-slider', get_template_directory_uri().'/flexslider.css', false, YO_VER);
    wp_enqueue_style('yo-lightbox', get_template_directory_uri().'/prettyPhoto.css', false, YO_VER);
    wp_enqueue_style('yo-style', get_template_directory_uri().'/style.css', false, YO_VER);


    // Main Javascript
    wp_register_script( 'yo-modernizr', get_template_directory_uri().'/js/libs/modernizr.custom.min.js', false, YO_VER);
    wp_register_script( 'yo-plugins', get_template_directory_uri().'/js/plugins.js', array('jquery'), YO_VER);

    wp_register_script( 'yo-script', get_template_directory_uri().'/js/script.js', array('jquery', 'yo-plugins'), YO_VER);
    wp_localize_script('yo-script', 'contact_data', array('ajaxurl' => admin_url('admin-ajax.php'), 'thank_you' => __('Thank you for contacting us!', 'theme_admin')));
    
    wp_enqueue_script( 'yo-modernizr' );
    wp_enqueue_script( 'yo-script' );

    // wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', false, YO_VER);
    //wp_enqueue_script('yo-modernizr', get_template_directory_uri().'/js/libs/modernizr.custom.min.js', false, YO_VER);
    //wp_enqueue_script('yo-plugins', get_template_directory_uri().'/js/plugins.js', array('jquery'), YO_VER);
    //wp_enqueue_script('yo-script', get_template_directory_uri().'/js/script.js', array('jquery'), YO_VER);

    // Enqueue the skin CSS file chosen in the theme options (if any)
    $style_options = get_field('style_options', 'option');
    if ($style_options == 'skins')
    {
        $color_scheme = get_field('color_scheme', 'option');
        if ( ! empty($color_scheme) && $color_scheme != 'dark')
            wp_enqueue_style('yo-color-scheme', get_template_directory_uri().'/style_'.$color_scheme.'.css', false, YO_VER);
    }
}
 
add_action('wp_enqueue_scripts', 'yo_enqueue_scripts');


/*
 *------------------------------------------------------------------------------------------------
 * Embed dynamic HTML, CSS & Javascript in the backend or frontend.
 *------------------------------------------------------------------------------------------------
 *
 */

// BACKEND
if (is_admin() && ( ! defined('DOING_AJAX') || ! DOING_AJAX))
{

}
// FRONTEND
else
{
    // Embed custom styles
    // $style_options = get_field('style_options', 'option');
    if (get_field('style_options', 'option') == 'custom')
    {
        $body = '';

        // Background
        $background = get_field('background', 'option');

        // Body background image
        if ( ! empty($background['image']))
            $body .= 'background-image: url("'.get_the_image($background['image']).'");';

        // Body background color
        if ( ! empty($background['color']))
            $body .= 'background-color: '.$background['color'].';';        

        // Body background repeat
        if ( ! empty($background['repeat']))
            $body .= 'background-repeat: '.$background['repeat'].';';

        // Body background position
        if ( ! empty($background['position']))
            $body .= 'background-position: '.$background['position'].';';

        // Body text
        $body_text = get_field('body_text', 'option');
        
        // Body text color        
        if ( ! empty($body_text['color']))
            $body .= 'color: '.$body_text['color'].';';   

        // Embed the body styles
        if ( ! empty($body))
            yo_css('body { '.$body.' }');

        // Text shadow
        if ( ! empty($body_text['shadow']))
            yo_css('h1,h2,h3,h4,p,#tagline { text-shadow: 0 1px 0 '.$body_text['shadow'].'; }');

        // Links color
        $links = get_field('links', 'option');
        if ( ! empty($links['color']))
            yo_css('a, a:link, a:active, a:visited { color: '.$links['color'].'; }');
        if ( ! empty($links['hover']))
            yo_css('a:hover { color: '.$links['hover'].'; }');

        // Logo & Tagline color
        $branding = get_field('branding', 'option');
        if ( ! empty($branding['logo_color']))
            yo_css('#logo a, #logo a:link, #logo a:active, #logo a:visited, #logo h1 { color: '.$branding['logo_color'].'; }');
        if ( ! empty($branding['tagline_color']))
            yo_css('#tagline { color: '.$branding['tagline_color'].'; }');

        // Buttons color
        $buttons = get_field('buttons', 'option');
        $bg_color = ( ! empty($buttons['bg_color'])) ? 'background-color:'.$buttons['bg_color'].';' : '';
        $bg_hover = ( ! empty($buttons['bg_hover'])) ? 'background-color:'.$buttons['bg_hover'].';' : '';
        $text_color = ( ! empty($buttons['text_color'])) ? 'color:'.$buttons['text_color'].';' : '';
        $text_shadow = ( ! empty($buttons['text_shadow'])) ? 'text-shadow:0 1px 0 '.$buttons['text_shadow'].';' : '';
        yo_css('.button, a.button, button, input[type="submit"] { '.$bg_color.$text_color.$text_shadow.' }');
        yo_css('.button:hover, a.button:hover, button:hover, input[type="submit"]:hover { '.$bg_hover.' }');
    }


    // Retina display logo
    $logo2x = get_field('logo2x', 'option');
    if ( ! empty($logo2x))
    {
        // $logo = get_field('logo', 'option');
        $image_attributes = wp_get_attachment_image_src($logo2x, 'full');

        // yo_css('.testsetset { '.get_the_image($logo2x).' }');

        yo_css('@media screen and (-webkit-min-device-pixel-ratio: 2), screen and (max--moz-device-pixel-ratio: 2) {
          #logo a img {
            visibility: hidden;
          }
          #logo a {
            background: url('.get_the_image($logo2x).') no-repeat;
            background-size: '.round($image_attributes[1]/2).'px '.round($image_attributes[2]/2).'px;
            padding: 3px 0;
          }
        }');
    }

    // Navigation top margin
    $main_nav_top = get_field('main_nav_top', 'option');
    if ( ! empty($main_nav_top))
        yo_css('#main-nav { top:'.$main_nav_top.'; }');
    
    // Tracking code
    $tracking_code = get_field('tracking_code', 'option');
    if ( ! empty($main_nav_top))
        yo_html($tracking_code, true);

    // Favicon2x
    $favicon2x = get_field('favicon2x', 'option');
    if ( ! empty($favicon2x))
    {
        yo_html('<link rel="apple-touch-icon" sizes="144x144" href="'.get_the_image($favicon2x).'"/>');
        yo_html('<link rel="apple-touch-icon" sizes="114x114" href="'.get_the_image($favicon2x).'"/>');
        yo_html('<link rel="apple-touch-icon" sizes="72x72" href="'.get_the_image($favicon2x).'"/>');
        yo_html('<link rel="apple-touch-icon" sizes="57x57" href="'.get_the_image($favicon2x).'"/>');
        yo_html('<link rel="icon" sizes="32x32" href="'.get_the_image($favicon2x).'"/>');
    }

    // Favicon
    $favicon = get_field('favicon', 'option');
    if ( ! empty($favicon))
        yo_html('<link rel="icon" type="'.get_post_mime_type($favicon).'" href="'.get_the_image($favicon).'">');


    // Custom CSS
    $custom_css = get_field('css', 'option');
    if ( ! empty($custom_css))
        yo_css($custom_css);

    // Custom Javascript
    $custom_js = get_field('js', 'option');
    if ( ! empty($custom_js))
        yo_js($custom_js, true);
}




/*
 *------------------------------------------------------------------------------------------------
 * Contact Form
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_ajax_contact()
{
    if( ! empty($_POST))
    {
        $name = stripslashes($_POST['name']);
        $email = trim($_POST['email']);
        $subject = stripslashes($_POST['subject']);
        $message = stripslashes($_POST['message']);

        $response = array();
        $errors = array();

        // Check name
        if(!$name)
            $errors['name'] = __('Please enter your name.', 'theme_admin');

        // Check email
        if(!$email)
            $errors['email'] = __('Please enter an e-mail address.', 'theme_admin');

        if($email && !is_email($email))
            $errors['email'] = __('Please enter a valid e-mail address.', 'theme_admin');

        // Check message
        if(!$message)
            $errors['message'] = __('Please leave a message.', 'theme_admin');


        if(empty($errors))
        {
            $contact_email = get_field('contact_email', 'theme_admin');
            if ( ! $contact_email) $contact_email = get_bloginfo('admin_email');

            $mail = mail($contact_email, $subject, $message,
             "From: ".$name." <".$email.">\r\n"
            ."Reply-To: ".$email."\r\n"
            ."X-Mailer: PHP/" . phpversion());

            if($mail)
                $response['success'] = true;
        }
        else
        {
            $response['success'] = false;
            $response['errors'] = $errors;
        }
    }

    // Returns the array in JSON format
    die(array2json($response));
}

add_action('wp_ajax_contact_form', 'yo_ajax_contact');
add_action('wp_ajax_nopriv_contact_form', 'yo_ajax_contact');





/*
 *------------------------------------------------------------------------------------------------
 * Theme Customizer Settings (WP3.4+)
 *------------------------------------------------------------------------------------------------
 *
 */


function andante_customize_register($wp_customize)
{
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport = 'postMessage';

    $options  = get_option(THEME_OPTIONS);
    $defaults = array();


    // Default Layout
    $wp_customize->add_section('andante_layout', array(
        'title'    => __('Layout', 'theme_admin'),
        'priority' => 50,
    ));

    $wp_customize->add_setting('andante_theme_options[theme_layout]', array(
        'type'              => 'option',
        // 'default'           => $defaults['theme_layout'],
        'sanitize_callback' => 'sanitize_key',
    ));

    $wp_customize->add_control('andante_theme_options[theme_layout]', array(
        'section'    => 'andante_layout',
        'type'       => 'radio',
        'choices'    => array('a' => 'A', 'b' => 'B'),
    ));
}

// add_action('customize_register', 'andante_customize_register');




$content_width = ''; // Fix for Theme-Check



/*
 *------------------------------------------------------------------------------------------------
 * Debug functions
 *------------------------------------------------------------------------------------------------
 *
 */

global $turbine_debug_stack;
$turbine_debug_stack = array();

// Short function d() for t_debug()
if (!function_exists('d')) { function d($var, $label = '', $verbose = false, $stack = false) { t_debug($var, $label, $verbose, $stack); } }

function t_debug($var, $label = '', $verbose = false, $stack = false)
{
    // Stop execution if not in debug mode
    if (defined('T_DEBUG') && T_DEBUG === false) return;

    // Only admins can see debug messages
    if (!current_user_can('manage_options')) return;

    global $turbine_debug_stack;

    $t = microtime(true);
    $micro = sprintf("%06d",($t - floor($t)) * 1000000);

    $debug  = '<div>';
    $debug .= '<h2><strong>'.$label.'</strong></h2>';
    $debug .= '<pre>';
    ob_start();
    if ($verbose) var_dump($var); else print_r($var);
    $var_content = ob_get_contents();
    ob_end_clean();
    $debug .= htmlspecialchars($var_content, ENT_QUOTES);
    $debug .= '</pre>';
    $debug .= '<small>';
    $debug .= '#'.count($turbine_debug_stack).' &mdash; ';
    $debug .=  date("l jS F \@\ g:i:s.".$micro." a", microtime(true));
    // if (class_exists('Turbine_UI')) $debug .= ' &mdash; Called by: ' . Turbine_UI::get_caller_class();
    $debug .= ' &mdash; Called by: ' . t_caller_class();
    if ($stack) $debug .= t_call_stack(debug_backtrace());
    $debug .= '</small>';
    $debug .= '</div><br><br><br>';

    $turbine_debug_stack[] = $debug;
}


function t_print_debug()
{
    global $turbine_debug_stack;
    foreach ($turbine_debug_stack as $debug)
        echo '<div style="background:#fff;color:#333;border-top:1px solid #333; padding:30px;">'.$debug.'</div>';
}
add_action('admin_footer', 't_print_debug');
add_action('wp_footer', 't_print_debug');

function t_call_stack($stacktrace)
{
    $output = '<br>'.str_repeat("=", 50) .'<br>';
    $i = 1;
    foreach($stacktrace as $node) {
        if (isset($node['file']) && isset($node['line']))
            $output .= "$i. <strong>".basename($node['file']) ."</strong>: " .$node['function'] ." (#" .$node['line'].")<br>";
        $i++;
    }

    return $output;
} 

function t_caller_class()
{
    $traces = debug_backtrace();
    return (isset($traces[2]['class'])) ? $traces[2]['class'] : null;
}

function t_log($msg = '')
{ 
    // Stop execution if not in debug mode
    if (defined('T_DEBUG') && T_DEBUG === false) return;
    
    // open file
    $fd = fopen(trailingslashit(realpath(dirname(__FILE__))).'turbine_log.txt', "a");
    //$fd = fopen(T_PATH.'turbine_log.txt', "a");
    // write string
    fwrite($fd, date("Y-m-d H:i:s").' - '.$msg . "\n");
    // close file
    fclose($fd);
}