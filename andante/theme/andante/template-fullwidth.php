<?php
/*
Template Name: Fullwidth
*/

get_header(); ?>

	<!-- Fullwidth Page -->
	<section id="content" role="main" class="container">
		
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    	<h1 class="span12"><?php the_title(); ?></h1>
    	<div class="span12 hr_pattern"></div>
    	
        <?php the_content(); ?>

        <?php endwhile; endif; ?>

	</section>

<?php get_footer(); ?>