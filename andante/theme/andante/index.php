<?php get_header(); ?>

      <!-- Content -->
      <section id="content" role="main" class="container">

            <?php 
              global $wp_query;

              // if (is_category() && !is_singular() && !isset($page_title))
              //   $page_title = get_the_category_by_ID(get_query_var('cat'));
              // if (!$page_title) $page_title = get_the_title($wp_query->get_queried_object_id());
              // if (!$page_title) $page_title = __('Blog Listing', 'theme_admin');

              $page_title = single_cat_title('', false);
              if (!$page_title) $page_title = single_month_title('', false);
              if (!$page_title) $page_title = get_the_title($wp_query->get_queried_object_id());
              if (!$page_title) $page_title = __('Blog Listing', 'theme_admin');
            ?>

            <h1 class="span8"><?php echo $page_title; ?></h1>
      
            <!-- Arrows navigation -->
            <div class="span4">
                <?php if (is_singular()) : ?>
                  <ul class="projects-nav">
                        <!-- Previous item -->
                        <?php if (get_previous_post()) : ?>
                        <li class="nav-prev"><?php previous_post_link('%link', '<i class="icon-arrow-left"></i>'); ?></li>
                        <?php endif; ?>
                        
                        <!-- Back to the overview -->
                        <?php $overview = (get_option('page_for_posts')) ? get_permalink(get_option('page_for_posts')) : site_url(); ?>
                        <li><a href="<?php echo $overview; ?>" title="<?php _e('Back to the overview', 'theme_admin'); ?>"><i class="icon-overview"></i></a></li>

                        <!-- Next item -->
                        <?php if (get_next_post()) : ?>
                        <li class="nav-next"><?php next_post_link('%link', '<i class="icon-arrow-right"></i>'); ?></li>
                        <?php endif; ?>
                  </ul>
                <?php endif; ?>
            </div>

            <div class="hr_pattern"></div>

            <!-- Posts -->
            <section class="span8 one-column">

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <!-- Post -->
                <?php
                  if ( ! get_post_format()) {
                       get_template_part('library/format-standard');
                  } else {
                       get_template_part('library/format-'.get_post_format());
                  }
                ?> <!-- / Post -->

                
                <?php if (is_singular()) : ?>

                  <?php wp_link_pages(); ?>

                  <!-- Post tags -->
                  <?php the_tags('<p>'.__('Tags: ', 'theme_admin'), ', ', '</p>'); ?>
                  
                  <!-- Comments -->
                  <?php comments_template(); ?>

                <?php endif; ?>


              <?php endwhile; ?>

              <!-- Next/Previous Pge -->
              <?php
                  echo previous_posts_link(__('&larr; Previous page &nbsp;&nbsp;&nbsp;', 'theme_admin'));
                  echo next_posts_link(__('Next page &rarr;', 'theme_admin'));
              ?>

              <?php endif; ?>

            </section> <!-- / Posts -->


            <!-- Sidebar -->
            <aside class="span4">
                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar')) : ?>
                <?php endif; ?>
            </aside> <!-- / Sidebar -->

      </section> <!-- / #content -->

<?php get_footer(); ?>