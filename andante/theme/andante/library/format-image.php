<?php if (is_singular()) : ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class('media'); ?>>
        <?php if (has_post_thumbnail()) the_post_thumbnail('full'); ?>
        <ul class="meta">
            <li><i class="icon-date"></i><time pubdate datetime="<?php the_time('c'); ?>"><?php echo get_the_date(); ?></time></li>
            <li><i class="icon-comments"></i><?php comments_number(); ?></li>
            <li>&mdash; <a href="#"><?php the_author_posts_link(); ?></a></li>
        </ul>
        <?php $the_content = wpautop(get_the_content()); $the_content = do_shortcode($the_content); if ( ! empty($the_content)) : ?>
          <div class="hr_small"></div>
          <p><?php echo $the_content; ?></p>
        <?php endif; ?>
    </article>

<?php else : ?>
    
    <article id="post-<?php the_ID(); ?>" <?php post_class('media'); ?>>
        <?php if (has_post_thumbnail()) : ?>
        <a title="<?php printf(__('Permanent Link to %s', 'theme_admin'), get_the_title()); ?>" href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('portfolio-details', array('alt' => get_the_title(), 'title' => get_the_title())); ?>
        </a>
    <?php endif; ?>
    </article>

<?php endif; ?>