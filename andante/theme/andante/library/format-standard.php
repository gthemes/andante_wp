<?php if (is_singular()) : ?>

    <article id="post-<?php the_ID(); ?>">
        <?php if (has_post_thumbnail()) : ?>
        <div id="details">
            <?php /*the_post_thumbnail('portfolio-details');*/ ?>
            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'portfolio-details'); 
                  echo '<img src="'.$image[0].'" alt="'.get_the_title().'">'; ?>
        </div>
        <?php endif; ?>
        <ul class="meta">
            <li><i class="icon-date"></i><time pubdate datetime="<?php the_time('c'); ?>"><?php echo get_the_date(); ?></time></li>
            <li><i class="icon-comments"></i><?php comments_number(); ?></li>
            <li>&mdash; <a href="#"><?php the_author_posts_link(); ?></a></li>
        </ul>
        <?php $the_content = wpautop(get_the_content()); $the_content = do_shortcode($the_content); if ( ! empty($the_content)) : ?>
          <div class="hr_small"></div>
          <p><?php echo $the_content; ?></p>
        <?php endif; ?>
    </article>

<?php else : ?>
    
    <article id="post-<?php the_ID(); ?>" <?php post_class('media'); ?>>
        <?php if (has_post_thumbnail()) : ?>
        <a class="object" title="<?php printf(__('Permanent Link to %s', 'theme_admin'), get_the_title()); ?>" href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('portfolio-3-columns'); ?>
        </a>
        <?php endif; ?>
        <div class="caption">
          <h3><?php the_title(); ?></h3>
          <ul class="meta">
            <li><i class="icon-date"></i><?php echo get_the_date(); ?></li>
            <li><i class="icon-comments"></i><?php comments_number(); ?></li>
            <li>&mdash; <a href="#"><?php the_author_posts_link(); ?></a></li>
          </ul>
          <div class="hr_small"></div>
          <p><?php the_excerpt(); ?></p>
          <p><a class="mini button" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php _e('More &rarr;', 'theme_admin'); ?></a></p>
        </div>
    </article>

<?php endif; ?>