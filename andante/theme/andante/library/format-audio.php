    <article id="post-<?php the_ID(); ?>" <?php post_class('media'); ?>>
        <div class="caption">
          <h3><?php the_title(); ?></h3>
          <ul class="meta">
            <li><i class="icon-date"></i><?php echo get_the_date(); ?></li>
            <li>&mdash; <a href="#"><?php the_author_posts_link(); ?></a></li>
          </ul>          
        </div>
        <div><?php the_content(); ?></div>
    </article>