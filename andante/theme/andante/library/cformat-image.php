<?php if (is_singular() && ! is_front_page()) : ?>
	
	<!-- Additional project links -->
	<?php $images = get_field('yo_project_material_image'); /*$images = unserialize($images[0]);*/
	if ( ! empty($images)) : 
	    foreach ($images as $image) : $image_resized = wp_get_attachment_image_src($image['media_item'], 'portfolio-details'); ?>
			  <?php if ( ! empty($image['url'])) : ?><a href="<?php echo $image['url']; ?>"><?php endif; ?>
	          	<img src="<?php echo $image_resized[0]; ?>" alt="<?php echo $image['title']; ?>">
			  <?php if ( ! empty($image['url'])) : ?></a><?php endif; ?>
	<?php endforeach; endif; ?>


<?php else : ?>


	<!-- Media Object - Image + Caption -->
	<div class="<?php echo $span; ?>">
	      <div class="media">
	      		<?php if (get_field('yo_portfolio_enlarge') == true) : ?>
                  <a class="object" data-hover="icon-zoom" href="<?php the_image(get_post_thumbnail_id()); ?>" data-rel="gallery[home]"><?php the_post_thumbnail('portfolio-'.$featured['columns'].'-columns', array('title' => get_the_title())); ?></a>
                <?php else: ?>
                  <a class="object" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('portfolio-'.$featured['columns'].'-columns', array('title' => get_the_title())); ?></a>
                <?php endif; ?>

	            <div class="caption">
	                  <h3><?php the_title(); ?></h3>
	                  <div class="hr_small"></div>
	                  <p><?php the_excerpt(); ?></p>
	                  <p><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="button mini"><?php _e('More &rarr;', 'theme_admin') ?></a></p>
	            </div>
	      </div>
	</div> <!-- / Media Object -->


<?php endif; ?>