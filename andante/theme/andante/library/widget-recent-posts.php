<?php
/*
Plugin Name: Better Recent Posts Widget
Plugin URI: http://pippinsplugins.com/better-recent-posts-widget
Description: Provides a better recent posts widget, including thumbnails, category, and number options
Version: 1.1.4
Author: Pippin Williamson
Author URI: http://pippinsplugins.com
Modified by: Giordano Piazza - www.giordanopiazza.com
*/


/**
 * Recent Posts Widget Class
 */
class YO_Better_Recent_Posts extends WP_Widget {


    /** constructor */
    function YO_Better_Recent_Posts() {
        parent::WP_Widget(false, $name = 'Better Recent Posts', array('description' => __('Displays recent posts.', 'theme_admin')));	
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {	
        extract($args);
		    global $posttypes;
        $title 			= apply_filters('widget_title', $instance['title']);
        $cat 			= apply_filters('widget_title', $instance['cat']);
        $number 		= apply_filters('widget_title', $instance['number']);
        $offset 		= apply_filters('widget_title', $instance['offset']);
        $thumbnail_size = apply_filters('widget_title', $instance['thumbnail_size']);
        $thumbnail_width = apply_filters('thumbnail_width', $instance['thumbnail_width']);
        $thumbnail_height = apply_filters('widget_title', $instance['thumbnail_height']);
        $thumbnail 		= $instance['thumbnail'];
        $posttype 		= $instance['posttype'];
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
							
							<?php
								global $post;
								$tmp_post = $post;
								
								// get the category IDs and place them in an array
								
								$args = 'numberposts=' . $number . '&offset=' . $offset . '&post_type=' . $posttype . '&cat=' . $cat;
								$myposts = get_posts( $args );
								foreach( $myposts as $post ) : setup_postdata($post); ?>

                <!-- Post -->
                <div class="media">
                  <?php if($thumbnail == true) { ?>
                  <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), array($thumbnail_width, $thumbnail_height)); ?>
                  <a class="object" href="<?php the_permalink(); ?>" style="width:<?php echo $thumbnail_width; ?>px; height:<?php echo $thumbnail_height; ?>px; overflow:hidden;"><?php /*get_post_thumbnail_id();*/ the_post_thumbnail('thumbnail'); /*echo $thumbnail_width; print_r($image);*/ ?></a>
                  <?php } ?>
                    <div class="caption">
                      <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                      <p><span><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' ago'; ?></span></p>
                    </div>
                </div> <!-- / Post -->

								<?php endforeach; ?>
								<?php $post = $tmp_post; ?>

              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {		
    		global $posttypes;
    		$instance = $old_instance;
    		$instance['title'] = strip_tags($new_instance['title']);
    		$instance['cat'] = strip_tags($new_instance['cat']);
    		$instance['number'] = strip_tags($new_instance['number']);
    		$instance['offset'] = strip_tags($new_instance['offset']);
        $instance['thumbnail_size'] = strip_tags($new_instance['thumbnail_size']);
        $instance['thumbnail_width'] = strip_tags($new_instance['thumbnail_width']);
    		$instance['thumbnail_height'] = strip_tags($new_instance['thumbnail_height']);
    		$instance['thumbnail'] = $new_instance['thumbnail'];
    		$instance['posttype'] = $new_instance['posttype'];
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {	

		    $posttypes = get_post_types('', 'objects');
	
        $title = (isset($instance['title'])) ? esc_attr($instance['title']) : '';
        $cat = (isset($instance['cat'])) ? esc_attr($instance['cat']) : '';
        $number = (isset($instance['number'])) ? esc_attr($instance['number']) : '';
        $offset = (isset($instance['offset'])) ? esc_attr($instance['offset']) : '';
        $thumbnail_size = (isset($instance['thumbnail_size'])) ? esc_attr($instance['thumbnail_size']) : '';
        $thumbnail_width = (isset($instance['thumbnail_width'])) ? esc_attr($instance['thumbnail_width']) : '';
        $thumbnail_height = (isset($instance['thumbnail_height'])) ? esc_attr($instance['thumbnail_height']) : '';
        $thumbnail = (isset($instance['thumbnail'])) ? esc_attr($instance['thumbnail']) : '';
		    $posttype	= (isset($instance['posttype'])) ? esc_attr($instance['posttype']) : '';
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'theme_admin'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
		<p>
          <label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Category IDs, separated by commas', 'theme_admin'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('cat'); ?>" name="<?php echo $this->get_field_name('cat'); ?>" type="text" value="<?php echo $cat; ?>" />
        </p>
		<p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number to Show:', 'theme_admin'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
		<p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Offset (the number of posts to skip):', 'theme_admin'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('offset'); ?>" name="<?php echo $this->get_field_name('offset'); ?>" type="text" value="<?php echo $offset; ?>" />
        </p>
		<p>
          <input id="<?php echo $this->get_field_id('thumbnail'); ?>" name="<?php echo $this->get_field_name('thumbnail'); ?>" type="checkbox" value="1" <?php checked( '1', $thumbnail ); ?>/>
          <label for="<?php echo $this->get_field_id('thumbnail'); ?>"><?php _e('Display thumbnails?', 'theme_admin'); ?></label> 
        </p>
		<p>
          <label for="<?php echo $this->get_field_id('thumbnail_width'); ?>"><?php _e('Width the thumbnails, e.g. <em>80</em> = 80px', 'theme_admin'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('thumbnail_width'); ?>" name="<?php echo $this->get_field_name('thumbnail_width'); ?>" type="text" value="<?php echo $thumbnail_width; ?>" />
        </p>
    <p>
          <label for="<?php echo $this->get_field_id('thumbnail_height'); ?>"><?php _e('Height the thumbnails, e.g. <em>80</em> = 80px', 'theme_admin'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('thumbnail_height'); ?>" name="<?php echo $this->get_field_name('thumbnail_height'); ?>" type="text" value="<?php echo $thumbnail_height; ?>" />
        </p>
		<p>	
			<label for="<?php echo $this->get_field_id('posttype'); ?>"><?php _e('Choose the Post Type to display', 'theme_admin'); ?></label> 
			<select name="<?php echo $this->get_field_name('posttype'); ?>" id="<?php echo $this->get_field_id('posttype'); ?>" class="widefat">
				<?php
				foreach ($posttypes as $option) {
					echo '<option value="' . $option->name . '" id="' . $option->name . '"', $posttype == $option->name ? ' selected="selected"' : '', '>', $option->name, '</option>';
				}
				?>
			</select>		
		</p>
        <?php 
    }


} // class utopian_recent_posts
// register Recent Posts widget
// add_action('widgets_init', create_function('', 'return register_widget("YO_Better_Recent_Posts");'));

?>