<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }


// Theme Options
$theme_options = array();


// General Settings
$theme_options[] = array(
    'name'   => 'General Settings',
    'id'     => 'generalsettings',
    'fields' => array(
        //----------------------------------------------------------------------------------------
        // Logo Toggle
        //----------------------------------------------------------------------------------------
        array(
            'heading'  => __('General Settings', 'theme_admin'),
            'name'      => __('Enable logo image', 'theme_admin'),
            'id'        => 'logo_enabled',
            'desc'      => __('If this option is disabled the blog title will be used instead.', 'theme_admin'),
            'type'      => 'checkbox',
            'toggle'    => '.logo, .logo2x',
            'class'     => 'yo-toggle',
            'std'       => 0
        ),
        array(
            'name'     => __('Logo', 'theme_admin'),
            'id'       => 'logo',
            'desc'     => __('This is the logo at the original size, leave it empty to use the default title text.', 'theme_admin'),
            'type'     => 'media',
            'thumb'    => 'full'
        ),
        array(
            'name'  => __('Logo for retina display', 'theme_admin'),
            'id'    => 'logo2x',
            'desc'  => __('Upload a logo image which is exactly the <strong>double size of the standard logo</strong>. Leave it empty to disable this feature.', 'theme_admin'),
            'type'  => 'media',
            'thumb' => 'full'
        ),
        array(
            'name'  => __('Main navigation position', 'theme_admin'),
            'id'    => 'main_nav_top',
            'desc'  => __('The top margin for the main navigation, useful to align the nav with the logo.', 'theme_admin'),
            'type'  => 'text',
            'std'   => '0px'
        ),
        array(
            'name'  => __('Standard Favicon', 'theme_admin'),
            'id'    => 'favicon',
            'desc'  => __("Upload a <strong>16px by 16px PNG/GIF/JPG/ICO</strong> image that will represent your website's favicon.", 'theme_admin'),
            'type'  => 'media',
            'thumb' => 'full'
        ),
        array(
            'name'  => __('Favicon for retina display', 'theme_admin'),
            'id'    => 'favicon2x',
            'desc'  => __("Upload a <strong>114px by 114px PNG</strong> image that will represent your website's favicon on retina displays (iPhone, iPad etc...).", 'theme_admin'),
            'type'  => 'media',
            'thumb' => 'full'
        ),
        array(
            'name'  => __('Contact Form Email Address', 'theme_admin'),
            'id'    => 'contact_email',
            'desc'  => __('You will receive messages from the contact form to this email address. Leave it empty to use the default admin address.', 'theme_admin'),
            'type'  => 'text',
        ),
        array(
            'name'  => __('Tracking Code', 'theme_admin'),
            'id'    => 'tracking_code',
            'desc'  => __('Paste here the tracking code for the statistics (Google Analytics or other provider).', 'theme_admin'),
            'type'  => 'textarea',
        ),
    )
);


// Style Options
$theme_options[] = array(
    'name'   => __('Style Options', 'theme_admin'),
    'id'     => 'style',
    'fields' => array(
        //----------------------------------------------------------------------------------------
        // Look and feel
        //----------------------------------------------------------------------------------------
        array(
            'heading'  => __('Style Options', 'theme_admin'),
            'name'      => __('Look and feel', 'theme_admin'),
            'id'        => 'style_options',
            'desc'      => __("Select a skin or choose the colors you prefer.", 'theme_admin'),
            'type'      => 'radio',
            'toggle'    => '.color_scheme | '.
                           '.branding, .background, .body_text, .links, .buttons',
            'options'   => array(
                'skins'     => __('Default skins', 'theme_admin'),
                'custom'    => __('Custom colors scheme', 'theme_admin')),
            'std'       => 'custom'
        ),
        array(
            'name'      => __('Color Scheme', 'theme_admin'),
            'id'        => 'color_scheme',
            'desc'      => __("You can choose one of the default color schemes.", 'theme_admin'),
            'type'      => 'radio',
            'mod'       => 'images',
            'options'   => array(
                'dark'      => get_template_directory_uri().'/img/admin/dark.jpg',
                'smokewine' => get_template_directory_uri().'/img/admin/smoke_wine.jpg',
                'forest'    => get_template_directory_uri().'/img/admin/forest.jpg',
                'light'     => get_template_directory_uri().'/img/admin/light.jpg'),
            'std'       => 'dark'
        ),
        //----------------------------------------------------------------------------------------
        // Background
        //----------------------------------------------------------------------------------------
        array(
            'name'      => __('Background', 'theme_admin'),
            'id'        => 'background',
            'desc'      => __('You can customize the background image and settings.', 'theme_admin'),
            'type'      => 'group',
            'class'     => 'project-material',
            'fields'    => array(
                array(
                    'id'    => 'image',
                    'type'  => 'media'
                ),
                array(
                    'name'      => __('Color', 'theme_admin'),
                    'id'        => 'color',
                    'type'      => 'color',
                    'std'       => '#1c1c1c'
                ),
                array(
                    'name'      => __('Repeat', 'theme_admin'),
                    'id'        => 'repeat',
                    'type'      => 'select',
                    'options'   => array(
                        'no-repeat' => 'No Repeat',
                        'repeat'    => 'Repeat',
                        'repeat-x'  => 'Repeat Horizontally',
                        'repeat-y'  => 'Repeat Vertically'),
                    'std'       => 'repeat'
                ),
                array(
                    'name'      => __('Position', 'theme_admin'),
                    'id'        => 'position',
                    'type'      => 'select',
                    'options'   => array(
                        'left'    => 'Left',
                        'center'  => 'Centered',
                        'right'   => 'Right'),
                        // 'full'    => 'Full Screen'),
                    'std'       => 'left'
                ),
            )
        ),
        //----------------------------------------------------------------------------------------
        // Logo & Tagline
        //----------------------------------------------------------------------------------------
        array(
            'name'      => __('Logo & Tagline', 'theme_admin'),
            'id'        => 'branding',
            'desc'      => __("If you're using a logo image only the tagline will be changed.", 'theme_admin'),
            'type'      => 'group',
            'fields'    => array(
                array(
                    'name'      => __('Logo Color', 'theme_admin'),
                    'id'        => 'logo_color',
                    'type'      => 'color',
                    'std'       => '#ffffff'
                ),
                array(
                    'name'      => __('Tagline Color', 'theme_admin'),
                    'id'        => 'tagline_color',
                    'type'      => 'color',
                    'std'       => '#c8c8c8'
                ),
            )
        ),
        array(
            'name'     => __('Body Text', 'theme_admin'),
            'id'       => 'body_text',
            'desc'     => __("The page's text color. For the 'Text Shadow' is recommended '#fff' for dark text and '#000' for light text.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'     => __('Text Color', 'theme_admin'),
                    'id'       => 'color',
                    'type'     => 'color',
                    'std'      => '#eeeeee'
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'shadow',
                    // 'desc'     => __("", 'theme_admin'),
                    'type'     => 'color',
                    'std'      => '#000000'
                ),
            )
        ),
        array(
            'name'     => __('Links', 'theme_admin'),
            'id'       => 'links',
            'desc'     => __("The links color.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'     => __('Links Color', 'theme_admin'),
                    'id'       => 'color',
                    'type'     => 'color',
                    'std'      => '#8ed8d2'
                ),
                array(
                    'name'     => __('Hover Color', 'theme_admin'),
                    'id'       => 'hover',
                    'type'     => 'color',
                    'std'      => '#307b77'
                ),
            )
        ),
        array(
            'name'     => __('Buttons', 'theme_admin'),
            'id'       => 'buttons',
            'desc'     => __("The buttons color. For the 'Text Shadow' is recommended '#fff' for dark text and '#000' for light text.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'     => __('Background Color', 'theme_admin'),
                    'id'       => 'bg_color',
                    'type'     => 'color',
                    'std'      => '#333333'
                ),
                array(
                    'name'     => __('Text Color', 'theme_admin'),
                    'id'       => 'text_color',
                    'type'     => 'color',
                    'std'      => '#ffffff'
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'text_shadow',
                    'type'     => 'color',
                    'std'      => '#000000'
                ),
                array(
                    'name'     => __('Hover Background Color', 'theme_admin'),
                    'id'       => 'bg_hover',
                    'type'     => 'color',
                    'std'      => '#ff5500'
                ),
            )
        )
    )
);


// Home Settings
$theme_options[] = array(
    'name'   => 'Home Settings',
    'id'     => 'homesettings',
    'fields' => array(
        array(
            'heading'   => __('Home Settings', 'theme_admin'),
            'name'      => __('Images slideshow', 'theme_admin'),
            'id'        => 'home_slider',
            'desc'      => __('Add any number of images to the slider. For each slide you can set the title, image, link and description. Drag them to change the order.', 'theme_admin'),
            'type'      => 'group',
            'class'     => 'project-material',
            'clone'     => true,
            'sortable'  => true,
            'empty'     => true,
            'fields'    => array(
                array(
                    'id'    => 'media_item',
                    'type'  => 'media'
                ),
                array(
                    'name'      => __('Title', 'theme_admin'),
                    'id'        => 'title',
                    'type'      => 'text',
                ),
                array(
                    'name'      => __('URL', 'theme_admin'),
                    'id'        => 'url',
                    'type'      => 'text',
                ),
                array(
                    'name'      => __('Caption', 'theme_admin'),
                    'id'        => 'caption',
                    'type'      => 'text',
                ),
                array(
                    'name'      => __('Caption type', 'theme_admin'),
                    'id'        => 'caption_type',
                    'type'      => 'select',
                    'std'       => 'small_caption',
                    'options'   => array(
                        ''               => __('No caption', 'theme_admin'),
                        'small-caption'  => __('Small bottom caption', 'theme_admin'),
                        'medium-caption' => __('Bottom caption full width', 'theme_admin'),
                        'big-caption'    => __('Large centered caption', 'theme_admin')
                    ),
                ),
            )
        ),
        //----------------------------------------------------------------------------------------
        // Second block toggle
        //----------------------------------------------------------------------------------------
        array(
            'name'      => __('Enable feature boxes', 'theme_admin'),
            'id'        => 'features_enabled',
            'desc'      => __('Enable/Disable the feature boxes in the homepage.', 'theme_admin'),
            'type'      => 'checkbox',
            'toggle'    => '.features',
            'class'     => 'yo-toggle',
            'std'       => 0
        ),
        array(
            'name'      => __('Feature boxes', 'theme_admin'),
            'id'        => 'features',
            'desc'      => __('These are the feature boxes in the home page. For each feature you can set the title, image and text. Drag them to change the order. To avoid breaking the design, <strong>the images size has to be 56x65px</strong> and you can add <strong>up to 4 items</strong>. Leave it empty if you wish to disable this feature.', 'theme_admin'),
            'type'      => 'group',
            'class'     => 'project-material',
            'clone'     => true,
            'max'       => 4,
            'sortable'  => true,
            'empty'     => true,
            'fields'    => array(
                array(
                    'id'    => 'media_item',
                    'type'  => 'media'
                ),
                array(
                    'name'      => __('Title', 'theme_admin'),
                    'id'        => 'title',
                    'type'      => 'text',
                ),
                array(
                    'name'      => __('Text', 'theme_admin'),
                    'id'        => 'text',
                    'type'      => 'text',
                ),
                array(
                    'name'      => __('Link', 'theme_admin'),
                    'id'        => 'link',
                    'type'      => 'text',
                ),
                array(
                    'name'      => __('Target', 'theme_admin'),
                    'id'        => 'link_target',
                    'type'      => 'select',
                    'std'       => '_self',
                    'options'   => array(
                        '_self'        => __('_self', 'theme_admin'),
                        '_blank'       => __('_blank', 'theme_admin'),
                        '_top'         => __('_top', 'theme_admin'),
                    ),
                ),
            )
        ),
        array(
            'id'        => 'featured_projects',
            'name'      => __('Featured Projects', 'theme_admin'),
            'desc'      => __('Enable/Disable the featured projects in the homepage.', 'theme_admin'),
            'type'      => 'group',
            'fields'    => array(
                //----------------------------------------------------------------------------------------
                // Sticky Feature
                //----------------------------------------------------------------------------------------
                array(
                    'id'        => 'enabled',
                    'name'      => __('Enable featured projects', 'theme_admin'),
                    'type'      => 'checkbox',
                    'toggle'    => '.featured_projects_title, .featured_projects_columns',
                    'class'     => 'yo-toggle',
                    'std'       => 1
                ),
                array(
                    'id'    => 'title',
                    'name'  => __('Title', 'theme_admin'),
                    'type'  => 'text',
                    'std'   => __('Featured Projects', 'theme_admin')
                ),
                array(
                    'id'        => 'columns',
                    'name'      => __('Columns', 'theme_admin'),
                    'desc'      => __('Choose the number of columns to display featured projects in the homepage.', 'theme_admin'),
                    'type'      => 'radio',
                    'mod'       => 'images',
                    'options'   => array(
                        '2'  => YO_IMG_URL.'options/2col.png',
                        '3'  => YO_IMG_URL.'options/3col.png',
                        '4'  => YO_IMG_URL.'options/4col.png'),
                    'std'       => '3'
                )
            )
        ),
    )
);



// Portfolio Settings
$theme_options[] = array(
    'name'   => __('Portfolio Settings', 'theme_admin'),
    'id'     => 'portfoliosettings',
    'fields' => array(
        array(
            'heading'   => __('Portfolio Settings', 'theme_admin'),
            'name'      => __('Columns', 'theme_admin'),
            'id'        => 'portfolio_columns',
            'desc'      => __('Choose the number of columns for the portfolio overview pages.', 'theme_admin'),
            'type'      => 'radio',
            'mod'       => 'images',
            'options'   => array(
                '2'  => YO_IMG_URL.'options/2col.png',
                '3'  => YO_IMG_URL.'options/3col.png',
                '4'  => YO_IMG_URL.'options/4col.png'),
            'std'       => '3'
        ),
    )
);



$theme_options[] = array(
    'name'   => __('Custom Code', 'theme_admin'),
    'id'     => 'custom_code',
    'fields' => array(
        array(
            'heading'  => __('Custom Code', 'theme_admin'),
            'name'     => __('Custom CSS', 'theme_admin'),
            'id'       => 'css',
            'desc'     => __("Write here the CSS code you wish to include in all pages. You don't need to add any style tags.", 'theme_admin'),
            'type'     => 'textarea'
        ),
        array(
            'name'     => __('Custom Javascript', 'theme_admin'),
            'id'       => 'js',
            'desc'     => __("Write here the Javascript code you wish to include in all pages. You don't need to add any script tags.", 'theme_admin'),
            'type'     => 'textarea'
        ),
    )
);


yo_register_options($theme_options);