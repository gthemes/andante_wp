<?php


/*
 *------------------------------------------------------------------------------------------------
 * Portfolio Custom Type
 *------------------------------------------------------------------------------------------------
 *
 * Register the custom post type 'Portfolio' to manage the projects like posts/pages. WP3+
 * 
 */

function yo_register_custom_type()
{
    // Set up the custom post type
    register_post_type('portfolio', // Post types: post, page, attachment, revision, nav_menu_item, custom_type
        array(
            'labels' => array(
                'name' => __('Portfolio', 'theme_admin'),
                'add_new' => __('Add New Project', 'theme_admin'),
                'add_new_item' => __('Add New Project', 'theme_admin'),
                'new_item' => __('New Project', 'theme_admin'),
                'edit_item' => __('Edit Project', 'theme_admin'),
                'view_item' => __('View Project', 'theme_admin'),
                'search_items' => __('Search Projects', 'theme_admin'),
                'not_found' => __('No Projects found', 'theme_admin'),
                'not_found_in_trash' => __('No Projects found in Trash', 'theme_admin'),
                'all_items' => __('All Projects', 'theme_admin'),
            ),
            'public' => true,
            'has_archive' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'hierarchical' => true,
            'capability_type' => 'post',
            'menu_position' => 20, // null (below Comments), 0 (below Media), 5 (below Posts), 20 (below Pages), 60 (below first separator) and 100 (below second separator)
            'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'excerpt', 'custom-fields', 'sticky'),
            // 'menu-icon' => '',
            'taxonomies' => array('portfolio-categories'),
            'rewrite' => true,
            // Uncomment the 'rewrite' line to change the slug
            // Note: Go to 'Settings > Permalinks' and save changes to avoid 404 errors
            // 'rewrite' => array( 'slug' => 'test' ),
        )
    );
}

add_action('init', 'yo_register_custom_type');

function yo_register_taxonomies()
{
    // Register the custom type categories (taxonomies)
    register_taxonomy(  
        'portfolio-categories', // Taxonomy ID
        'portfolio',
        array(
            'labels' => array(
                'name' => __( 'Portfolio Categories', 'theme_admin' ),
                'all_items' => __('All Projects', 'theme_admin'),
                'menu_name' => __( 'Categories', 'theme_admin' )
            ),
            'public' => true,
            'hierarchical' => true, 
            'query_var' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'rewrite' => array('slug' => 'categories', 'hierarchical' => true),
            // 'capabilities' => 'edit_posts'
        )  
    );
}

add_action('init', 'yo_register_taxonomies');



/*
 *------------------------------------------------------------------------------------------------
 * Meta Boxes Definitions
 *------------------------------------------------------------------------------------------------
 *
 * All the meta boxes are defined here.
 * 
 */

// The fields for the 'Project details' meta box
$project_details = array(
    //----------------------------------------------------------------------------------------
    // First block header
    //----------------------------------------------------------------------------------------
    array(
        'heading'   => __('Description', 'theme_admin'),
        'name'      => __('Header text', 'theme_admin'),
        'id'        => YO_PREFIX.'header_1',
        'desc'      => __('The title for the main description paragraph.', 'theme_admin'),
        'type'      => 'text',
        // 'std'       => __('Overview', 'theme_admin')
    ),
    //----------------------------------------------------------------------------------------
    // Project date
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Date', 'theme_admin'),
        'id'        => YO_PREFIX.'project_date',
        'desc'      => __('The project launch date. (yy-mm-dd)', 'theme_admin'),
        'type'      => 'date',
        // 'format'    => 'yy-mm-dd'
        'format'    => 'dd-mm-yy'
    ),
    //----------------------------------------------------------------------------------------
    // Main project link
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Main project link', 'theme_admin'),
        'id'        => YO_PREFIX.'project_link',
        'desc'      => __('Set the label and URL for the big launch button in the project page, it can be a link to a web page or files.', 'theme_admin'),
        'type'      => 'group',
        'class'     => 'project-material',
        'fields'    => array(
            array(
                'name'  => __('Button label', 'theme_admin'),
                'id'    => 'label',
                'type'  => 'text',
                'std'   => __('Launch &rarr;', 'theme_admin'),
            ),
            array(
                'name'  => __('Button link', 'theme_admin'),
                'id'    => 'url',
                'type'  => 'text'
            )
        )
    ),
    //----------------------------------------------------------------------------------------
    // Additional project links
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Additional project links', 'theme_admin'),
        'id'        => YO_PREFIX.'project_links',
        'desc'      => __('Add any number of URLs with text. Drag them to change the order. (example: client name - http://www.clienturl.com)', 'theme_admin'),
        'type'      => 'group',
        'class'     => 'project-material',
        'clone'     => true,
        'sortable'  => true,
        'empty'     => true,
        'fields'    => array(
            //----------------------------------------------------------------------------------------
            // Text
            //----------------------------------------------------------------------------------------
            array(
                'name'      => __('Link text', 'theme_admin'),
                'id'        => YO_PREFIX.'link_text',
                'type'      => 'text'
            ),
            //----------------------------------------------------------------------------------------
            // URL
            //----------------------------------------------------------------------------------------
            array(
                'name'      => __('Link URL', 'theme_admin'),
                'id'        => YO_PREFIX.'link_url',
                'type'      => 'text'
            ),
        )
    ),
    //----------------------------------------------------------------------------------------
    // Paragraph
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Paragraph text', 'theme_admin'),
        'id'        => YO_PREFIX.'paragraph_1',
        'desc'      => __('This is the main description for the project. You can customize it as you wish using the editor features.', 'theme_admin'),
        'type'      => 'wysiwyg',
    ),
    //----------------------------------------------------------------------------------------
    // Second block toggle
    //----------------------------------------------------------------------------------------
    array(
        'heading'   => __('Additional description', 'theme_admin'),
        'name'      => __('Toggle the second description block', 'theme_admin'),
        'id'        => YO_PREFIX.'block_toggle',
        'desc'      => __('Enable/Disable the second block of text', 'theme_admin'),
        'type'      => 'checkbox',
        'toggle'    => '.'.YO_PREFIX.'header_2, .'.YO_PREFIX.'paragraph_2',
        'class'     => 'yo-toggle',
        'std'       => 0
    ),
    //----------------------------------------------------------------------------------------
    // Second block header
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Header text', 'theme_admin'),
        'id'        => YO_PREFIX.'header_2',
        'desc'      => __('The title for the second paragraph.', 'theme_admin'),
        'type'      => 'text',
        // 'std'       => __("What we've done", 'theme_admin')
    ),
    //----------------------------------------------------------------------------------------
    // Paragraph
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Paragraph text', 'theme_admin'),
        'id'        => YO_PREFIX.'paragraph_2',
        'desc'      => __('The paragraph text for the second block.', 'theme_admin'),
        'type'      => 'wysiwyg',
    ),
);

// The fields for the 'Project material' meta box
$project_material = array(
    //----------------------------------------------------------------------------------------
    // Content Type
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Content type', 'theme_admin'),
        'id'        => YO_PREFIX.'content_type',
        'desc'      => __("Select the type of content for this project.", 'theme_admin'),
        'type'      => 'radio',
        'toggle'    => '.'.YO_PREFIX.'project_material_image | '.
                       '.'.YO_PREFIX.'project_material_slideshow | '.
                       '.'.YO_PREFIX.'project_material_video | '.
                       '.'.YO_PREFIX.'project_material_audio',
        'options'   => array(
            'image'     => __('Image', 'theme_admin'),
            'slideshow' => __('Slideshow', 'theme_admin'),
            'video'     => __('Video', 'theme_admin'),
            'audio'     => __('Audio', 'theme_admin')),
        'std'       => 'image'
    ),
    //----------------------------------------------------------------------------------------
    // Images
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Images', 'theme_admin'),
        'id'        => YO_PREFIX.'project_material_image',
        'desc'      => __('You can add as many images as you wish, for each one you can set the title, image, and link. Drag them to change the order.', 'theme_admin'),
        'type'      => 'group',
        'class'     => 'project-material',
        'clone'     => true,
        'sortable'  => true,
        'empty'     => true,
        'fields'    => array(
            array(
                'id'    => 'media_item',
                'type'  => 'media'
            ),
            array(
                'name'      => __('Title', 'theme_admin'),
                'id'        => 'title',
                'type'      => 'text',
            ),
            array(
                'name'      => __('URL', 'theme_admin'),
                'id'        => 'url',
                'type'      => 'text',
            ),         
        )
    ),
    //----------------------------------------------------------------------------------------
    // Images slideshow
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Images slideshow', 'theme_admin'),
        'id'        => YO_PREFIX.'project_material_slideshow',
        'desc'      => __('Add any number of images to the slider. For each slide you can set the title, image, link and description. Drag them to change the order.', 'theme_admin'),
        'type'      => 'group',
        'class'     => 'project-material',
        'clone'     => true,
        'sortable'  => true,
        'empty'     => true,
        'fields'    => array(
            array(
                'id'    => 'media_item',
                'type'  => 'media'
            ),
            array(
                'name'      => __('Title', 'theme_admin'),
                'id'        => 'title',
                'type'      => 'text',
            ),
            array(
                'name'      => __('URL', 'theme_admin'),
                'id'        => 'url',
                'type'      => 'text',
            ),
        )
    ),
    //----------------------------------------------------------------------------------------
    // Video embed
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Video embed', 'theme_admin'),
        'id'        => YO_PREFIX.'project_material_video',
        'desc'      => __("Copy and paste the code from any website with the 'Share' feature like YouTube, Vimeo, Ustream, MetaCafe and most of the video-related websites around. Usually it's found with a 'share' or 'embed' button", 'theme_admin'),
        'type'      => 'textarea'
    ),
    //----------------------------------------------------------------------------------------
    // Audio embed
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Audio embed', 'theme_admin'),
        'id'        => YO_PREFIX.'project_material_audio',
        'desc'      => __("Copy and paste the code from any website with the 'Share' feature like SoundCloud and most of the audio-related websites around. Usually it's found with a 'share' or 'embed' button", 'theme_admin'),
        'type'      => 'textarea',
    ),
);




// Metaboxes
$metaboxes = array();

// Project details
$metaboxes[] = array(
    'id' => 'project-details',
    'title' => __('Project Details', 'theme_admin'),
    'pages' => array('portfolio'),
    'context' => 'normal',
    'priority' => 'high',
    'fields' => $project_details
);

// Project material
$metaboxes[] = array(
    'id' => 'project-material',
    'title' => __('Project Material', 'theme_admin'),
    'pages' => array('portfolio'),
    'context' => 'normal',
    'priority' => 'high',
    'fields' => $project_material
);

// Featured Project (sticky feature)
$metaboxes[] = array(
    'id' => 'project-sticky',
    'title' => __('Featured Project', 'theme_admin'),
    'pages' => array('portfolio'),
    'context' => 'side',
    'priority' => 'default',
    'fields' => array(
        array(
            'id'    => 'is_sticky',
            'name'  => __('Show in the home page', 'theme_admin'),
            'type'  => 'checkbox',
            'std'   => 0,
            'class' => 'yo-oneline'
        ),
        array(
            'id'    => 'is_sticky_sidebar',
            'name'  => __('Show in the sidebar', 'theme_admin'),
            'type'  => 'checkbox',
            'std'   => 0,
            'class' => 'yo-oneline'
        )
    )
);

// Enlarge Thumbnails
$metaboxes[] = array(
    'id'        => 'project-enlarge',
    'title'     => __('Enlarge Featured Image', 'theme_admin'),
    'pages'     => array('portfolio'),
    'context'   => 'side',
    'priority'  => 'default',
    'fields'    => array(
        array(
            'name'  => __('Enlarge Thumbnails / Link to project', 'theme_admin'),
            'id'    => YO_PREFIX.'portfolio_enlarge',
            'desc'  => __('Show the larger image when clicking on the project thumbnail. If disabled, link to the project.', 'theme_admin'),
            'type'  => 'checkbox',
            'std'   => 0,
            'class' => 'yo-oneline'
        )
    )
);

yo_register_metabox($metaboxes);




/*
 *------------------------------------------------------------------------------------------------
 * Custom Overview Columns
 *------------------------------------------------------------------------------------------------
 *
 * Add custom columns in the posts overview.
 * 
 */

function add_new_columns($defaults)
{
    $defaults['portfolio_cats'] = __('Project Categories', 'theme_admin');
    return $defaults;
}

function add_column_data($column_name, $post_id)
{
    global $post;

    switch ($column_name)
    {
        case 'portfolio_cats':
            $_taxonomy = 'portfolio-categories';
            $terms = get_the_terms($post_id, $_taxonomy);

            if ( ! empty($terms))
            {
                $out = array();
                foreach ($terms as $c)
                    $out[] = "<a href='edit-tags.php?action=edit&taxonomy=$_taxonomy&post_type=portfolio&tag_ID={$c->term_id}'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'category', 'display')) . "</a>";
                echo join(', ', $out);
            }
            else {
                _e('Uncategorized', 'theme_admin');
            }
        break;        
    }
}

add_filter('manage_portfolio_posts_columns', 'add_new_columns');
add_action('manage_portfolio_posts_custom_column', 'add_column_data', 10, 2);



/*
 *------------------------------------------------------------------------------------------------
 * Add Custom Type to Feed
 *------------------------------------------------------------------------------------------------
 *
 * Add a Custom Post Type to the RSS feed
 * 
 */

function add_cpt_feed($qv) {
    if (isset($qv['feed']) && !isset($qv['post_type']))
        $qv['post_type'] = array('post', 'portfolio');
    return $qv;
}
add_filter('request', 'add_cpt_feed');




/*
 *------------------------------------------------------------------------------------------------
 * Sort Custom Type
 *------------------------------------------------------------------------------------------------
 *
 * Add a sub-page in the custom post type to sort the items.
 * 
 */

function yo_sort_portfolio_menu()
{
    add_submenu_page('edit.php?post_type=portfolio', __('Sort Projects', 'theme_admin'), __('Sort Projects', 'theme_admin'), 'edit_posts', basename(__FILE__), 'yo_sort_portfolio'); // 100% safe - ignore Theme-Check nag
}

add_action('admin_menu' , 'yo_sort_portfolio_menu'); 


function yo_sort_portfolio() {
    $projects = new WP_Query('post_type=portfolio&posts_per_page=-1&orderby=menu_order&order=ASC');

    echo '<div class="wrap">';
    echo '<h3>'.__('Sort Projects', 'theme_admin').' <img src="'.get_bloginfo('url').'/wp-admin/images/loading.gif" id="loading-animation" style="display:none;" /></h3>';
    echo '<p>'.__('Drag the projects to sort them.', 'theme_admin').'</p>';

    echo '<div id="portfolio-list">';
    while ($projects->have_posts()) : $projects->the_post();
        $title = get_the_title();
        $title = ( ! empty($title)) ? $title : __('(no title)', 'theme_admin');
        echo '<div class="postbox" id="'.get_the_id().'">';
        echo '<div class="sortable-type">';
        echo get_the_post_thumbnail(get_the_id(), 'thumbnail');
        echo '<h4>'.$title.'</h4></div></div>';
    endwhile;

    echo '</div></div>';
}

function yo_save_portfolio_order() {
    global $wpdb; // WordPress database class
 
    $order = explode(',', $_POST['order']);
    $counter = 0;
 
    foreach ($order as $new_id) {
        $wpdb->update($wpdb->posts, array( 'menu_order' => $counter ), array( 'ID' => $new_id) );
        $counter++;
    }
    die(1);
}
add_action('wp_ajax_sort_portfolio', 'yo_save_portfolio_order');



// Add custom column in the posts overview
// add_action("manage_posts_custom_column", "custom_columns");
// add_filter("manage_edit-portfolio_columns", "portfolio_columns");

// function portfolio_columns($columns)
// {
//     $columns = array(
//         "cb" => "<input type=\"checkbox\" />",
//         "title" => "Podcast Title",
//         // "description" => "Description",
//         "length" => "Length",
//         "category" => "Categories",
//         "comments" => 'Comments'
//     );
//     return $columns;
// }

// function custom_columns($column)
// {
//     global $post;
//     if ("ID" == $column) echo $post->ID;
//     // elseif ("description" == $column) echo $post->post_content;
//     elseif ("length" == $column) echo "63:50";
//     elseif ("speakers" == $column) echo "Test";
// }