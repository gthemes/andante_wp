<?php $post_images = yo_extract_images(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('media'); ?>>
	<div class="content-slider compact-nav">
	  	<div class="flexslider">
			<ul class="slides">

				<!-- Slides -->
				<?php foreach ($post_images as $image) :  ?>
					<li><?php echo $image; ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</article>