<?php 
/*
 Plugin Name: Projects Widget
 Plugin URI: http://www.giordanopiazza.com
 Version: 1.0.0
 Author: Giordano Piazza
 Author URI: http://www.giordanopiazza.com 
 */

defined('ABSPATH') or die("Cannot access pages directly.");

add_action( 'widgets_init', create_function('', 'return register_widget("Projects_Widget");') );


/**
 * 
 * @author gyo
 * Feature Widget
 */
class Projects_Widget extends WP_Widget
{
  function Projects_Widget()
  {
    $widget_ops = array('classname' => 'Projects_Widget', 'description' => __('This widget displays a random project from the portfolio.', 'theme_admin'));
    $this->WP_Widget('Projects_Widget', 'Featured Projects', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
    	echo $before_title . $title . $after_title;;
 
    $args = array('post_type' => 'portfolio',
    			  'post_status' => 'publish',
    			  'orderby' => 'rand',
    			  'posts_per_page' => 1,
    			  'meta_query' => array('relation' => 'AND', array('key' => 'is_sticky_sidebar', 'value' => '1', 'compare' => '=', 'type' => 'CHAR')));
    $featured_project = new WP_Query($args);


	if ($featured_project->have_posts()) : while ($featured_project->have_posts()) : $featured_project->the_post();
?>
		<!-- Media Object - Image + Caption -->
		<div class="media">
		    <a class="object" href="<?php the_permalink(); ?>"><img src="<?php the_image(get_post_thumbnail_id(get_the_ID()), 'portfolio-3-columns'); ?>" alt="<?php the_title(); ?>"></a>
		    <div class="caption">
				<h3><?php the_title(); ?></h3>
				<div class="hr_small"></div>
				<p><?php the_excerpt(); ?></p>
				<p><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="button mini"><?php _e('More &rarr;', 'theme_admin') ?></a></p>
		    </div>
		</div> <!-- / Media Object -->

<?php      
    endwhile; endif;    
  }
 
}






