<!-- <article id="post-<?php the_ID(); ?>" <?php post_class('media'); ?>>
    <h3><a title="<?php the_title(); ?>" href="<?php echo strip_tags(get_the_content()); ?>" target="_blank"><?php the_title(); ?> &rarr;</a></h3>
</article> -->


    <article id="post-<?php the_ID(); ?>" <?php post_class('media'); ?>>
        <div class="caption">
          <h3><a title="<?php the_title(); ?>" href="<?php echo strip_tags(get_the_content()); ?>" target="_blank"><?php the_title(); ?> &rarr;</a></h3>
          <ul class="meta">
            <li><i class="icon-date"></i><?php echo get_the_date(); ?></li>
            <li>&mdash; <a href="#"><?php the_author_posts_link(); ?></a></li>
          </ul>          
        </div>
    </article>