              <div class="content-slider compact-nav">
                  <div class="flexslider">
                        <ul class="slides">
                        <?php $slides = get_field('yo_project_material_slideshow');
                          if ( ! empty($slides)): ?>
                        <?php foreach ($slides as $slide) : ?>
                            <li>
                                <?php if ( ! empty($slide['url'])) : ?><a href="<?php echo $slide['url']; ?>"><?php endif; ?>
                                <img src="<?php the_image($slide['media_item']); ?>" alt="<?php echo $slide['title']; ?>" />
                                <?php if ( ! empty($slide['url'])) : ?></a><?php endif; ?>
                            </li>
                        <?php endforeach; endif; ?>
                        </ul>
                  </div>
              </div>