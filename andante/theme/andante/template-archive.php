<?php
/*
Template Name: Archives
*/

get_header(); ?>

      <!-- Content -->
      <section id="content" role="main" class="container">

            <h1 class="span12"><?php the_title(); ?></h1>
            <div class="span12 hr_pattern"></div>

            <!-- Posts -->
            <section class="span8 one-column">
                  
                  <!-- Start the Loop. -->
                  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                  
                  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                  <!-- Display the Post's Content in a div box. -->
                  <div class="entry">
                  <?php the_content(); ?>
                  </div>

                  </div> <!-- closes the first div box -->

                  <!-- Stop The Loop -->
                  <?php endwhile; ?>
                
                  <h3><?php _e('Last 5 Posts', 'theme_admin'); ?></h3>

                  <ul>
                    <?php
                    $recentposts = get_posts('numberposts=5');
                      foreach ($recentposts as $post) :
                        setup_postdata($post);
                    ?>
                    <li><i class="icon-add"></i><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
                    <?php endforeach; ?>
                  </ul>

                  <div class="hr_pattern"></div>

                  <h3><?php _e('Archives by Month', 'theme_admin'); ?></h3>

                  <ul>
                  <?php 
                    $args = array(
                        'type'            => 'monthly',
                        'limit'           => 6,
                        'format'          => 'html', 
                        'before'          => '<i class="icon-add"></i>',
                        'after'           => '',
                        'show_post_count' => true,
                        'echo'            => 1
                    );

                    wp_get_archives($args);
                  ?>
                  </ul>

                  <?php else: ?>

                  <p><?php _e('Sorry, no pages matched your criteria.', 'theme_admin'); ?></p>

                  <!-- REALLY stop The Loop. -->
                  <?php endif; ?>
            </section> <!-- / Posts -->


            <!-- Sidebar -->
            <aside class="span4">
                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar')) : ?>
                <?php endif; ?>
            </aside> <!-- / Sidebar -->

      </section> <!-- / #content -->

<?php get_footer(); ?>