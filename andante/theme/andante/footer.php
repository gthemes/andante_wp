	<?php if (function_exists('dynamic_sidebar') && is_active_sidebar('above-footer-widgets')) : ?>
            <div class="container">
            	<div class="span12 hr_pattern"></div>
                <?php dynamic_sidebar('above-footer-widgets'); ?>
            </div>
      <?php endif; ?>

	<!-- Footer -->
    <footer id="main-footer">

    	<?php if (function_exists('dynamic_sidebar') && is_active_sidebar('footer-widgets')) : ?>
            <div class="container">
                <?php dynamic_sidebar('footer-widgets'); ?>
            </div>
      	<?php endif; ?>

    </footer> <!-- / #main-footer -->

<?php wp_footer(); ?>
</body>
</html>