============================================================

THEME INSTALLATION

============================================================

You can find the theme file inside the 'theme folder' of the package you downloaded.

There are two ways to install it:
- FTP Upload: uncompress the theme file and upload the extracted folder to your server in the path wp-content/themes/.

- Wordpress Admin: go to Appearance > Install Themes > Upload, then browse for the zip theme file and click Install Now.
When the upload has finished, go to Appearance > Themes and activate this theme.





============================================================

DUMMY DATA

============================================================

To import the example data go to Tools > Import and install the Wordpress importer.
Once the install has finished, go back to Tools > Import > Wordpress and browse for the file 'dummy_data.xml' located in the 'theme' folder.
The dummy data will only import the posts, pages and portfolio items along with the attached images.
However, it won't import menus, widgets and theme options.